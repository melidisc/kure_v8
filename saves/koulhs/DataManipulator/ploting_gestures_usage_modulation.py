import pickle
import numpy as np
import os,sys
import matplotlib.pyplot as plt
from matplotlib import rc
from dtw import dtw

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def robot_plot(efile):
    experts_dataset = pickle.load(efile)
    print "Loaded ",len(experts_dataset), " experts."
    return experts_dataset
def user_plot(efile):
    path = efile
    print "Loading file at ", path
    data = pickle.load(open(path,"rb"))
    print "Loaded ", len(data)," entries"
    return data

def dtw_dist(A, B):
    '''
        MD-TDW,
    '''
    return np.sum(np.abs(A-B))
    # return np.sqrt(np.sum(np.square(A-B)))


#load the esn data
# esn_out = user_plot(the_path,"training","IN")
# plt.plot(esn_out)
# plt.show()
# esn_out = user_plot(the_path,"training","OUT")
# plt.plot(esn_out)
# plt.show()

plt.tick_params(axis='x', labelsize=16)
plt.tick_params(axis='y', labelsize=16)

training_in = user_plot("./training_ploting/user_ann_training_dataIN.pickle")
forward_tr = training_in[:58]
left_tr = training_in[58:119]
right_tr = training_in[119:]
moves_tr = [forward_tr,left_tr,right_tr]

sn_in = user_plot("./usage_plotting/user_ann_use_in_data.pickle")

esn_out = user_plot("./usage_plotting/user_ann_use_out_data.pickle")
experts = np.zeros((len(esn_out),2))
f=np.array([[1.],[1.]])
r=np.array([[1.],[-1.]])
l=np.array([[-1.],[1.]])

experts = 1/3.*np.array(esn_out)[:,0]*f \
        + 1/3.*np.array(esn_out)[:,5]*l \
        + 1/3.*np.array(esn_out)[:,6]*r
experts = experts.transpose()
N=100
p1 = np.convolve(experts[:,0], np.ones((N,))/N, mode='valid')
p2 = np.convolve(experts[:,1], np.ones((N,))/N, mode='valid')

p = np.column_stack((p1,p2))

conv_inp = np.array(sn_in)
c_stack = []
for i in xrange(6):
    ci = np.convolve(conv_inp[:,i], np.ones((N,))/N, mode='valid')
    c_stack.append(ci)

sn_in = c_stack[0]
for c in c_stack[1:]:
    sn_in = np.column_stack((sn_in,c))


behaviours = []
esn_in = []
#steep
# behaviours.append(p[200:530])
behaviours.append(p[200:400])
esn_in.append(sn_in[200:400])
#less steep
behaviours.append(p[1220:1420])
esn_in.append(sn_in[1220:1420])
#forward
# behaviours.append(p[4050:4500])
behaviours.append(p[4200:4500])
esn_in.append(sn_in[4200:4500])
#modulation
behaviours.append(p[5300:5550])
esn_in.append(sn_in[5300:5550])
#stopping
behaviours.append(p[7900:8250])
esn_in.append(sn_in[7900:8250])
#Backwards
behaviours.append(p[11800:12300])
esn_in.append(sn_in[11800:12300])


# import pdb; pdb.set_trace()
tites = ["Steep Turn", "Gentle turn","Forward",
        "Modulation", "Stopping","Backward"]
for t,vz in zip(tites,zip(esn_in,behaviours)):
    _in, _out = vz
    f, axarr = plt.subplots(2, sharex=True)
    f.canvas.set_window_title(t)
    names = ["Right Wheel Velocity", "Left Wheel Velocity"]
    ln_shape = ["--","-."]
    tmp1 = np.array(_out).transpose()
    for idn,g in enumerate(tmp1):
        axarr[0].plot(g,ln_shape[idn],label=names[idn],linewidth=1., alpha=0.5)
    axarr[0].set_title("Robot Behaviour")
    axarr[0].set_ylabel('Value',fontsize=16)
    axarr[0].legend()
    if t =="Forward":
        axarr[0].set_ylim([-.2,.8])
    # axarr[0].tight_layout()
    # plt.show()
    # sys.exit()

    # plt.figure()

    names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]

    tmp1 = np.array(_in).transpose()
    for idn,g in enumerate(tmp1):
        axarr[1].plot(g,ln_shape[idn],label=names[idn],linewidth=1.0, alpha=0.5)
    axarr[1].set_title("Input Gesture")
    axarr[1].set_xlabel('Time',fontsize=16)
    axarr[1].set_ylabel('Value',fontsize=16)
    axarr[1].legend()
    axarr[1].set_ylim([-1.2,1.2])
    plt.tight_layout()

    DTWz = np.array([dtw(move, _in, dist = dtw_dist)[0] for move in moves_tr])
    print "Move ",t, \
        " dtw to F,L,R ", \
        DTWz

    speedz = np.array([[1.,1.],[1.,-1.],[-1.,1.]])
    # print np.array(DTWz).shape, speedz.shape
    # print np.dot(DTWz.transpose(),speedz)
    # speed = 0
    # for dz in DTW:
    #     speed +=
    # plt.
    plt.savefig(t+"_usage_modulation.eps")


plt.show()

sys.exit()
titles = ["Move Forward", "Turn Left", "Turn Right"]
for idx,gst in enumerate(tmp):
    plt.figure()
    gst = np.array(gst).transpose()
    names = ["heaving", "swaying", "surging", "pitching", "yawing", "rolling"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]
    for idn,g in enumerate(gst):
        plt.plot(g,ln_shape[idn],label=names[idn],linewidth=2.0, alpha=0.5)
    plt.title(titles[idx])
    plt.xlabel('Time',fontsize=16)
    plt.ylabel('Value',fontsize=16)
    # plt.axvline(x=60, alpha=0.3)
    # plt.axvline(x=120, alpha=0.3)
    # plt.axvline(x=180, alpha=0.3)
    # plt.axvspan(90,155, alpha=0.2, color="g")
    # plt.axvline(x=200)
    plt.legend()
    plt.tight_layout()
    plt.savefig(titles[idx]+".eps")

# plt.figure()
# esn_out = user_plot(the_path,"use_in","")
# plt.plot(esn_out)
plt.show()
