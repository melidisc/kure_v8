import pickle
import numpy as np
import os,sys
import matplotlib.pyplot as plt
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def robot_plot(efile):
    experts_dataset = pickle.load(efile)
    print "Loaded ",len(experts_dataset), " experts."
    return experts_dataset
def user_plot(efile):
    path = efile
    print "Loading file at ", path
    data = pickle.load(open(path,"rb"))
    print "Loaded ", len(data)," entries"
    return data


#load the esn data
# esn_out = user_plot(the_path,"training","IN")
# plt.plot(esn_out)
# plt.show()
# esn_out = user_plot(the_path,"training","OUT")
# plt.plot(esn_out)
# plt.show()

plt.tick_params(axis='x', labelsize=16)
plt.tick_params(axis='y', labelsize=16)

esn_out = user_plot("user_ann_training_dataIN.pickle")

tmp = []
tmp1 = []
tmp.append(esn_out[:58])
tmp.append(esn_out[58:119])
tmp.append(esn_out[119:])

tmp1.extend(np.zeros_like(esn_out[:18])+np.array(esn_out[:18])*.01)
tmp1.extend(esn_out[:58])
tmp1.extend(np.zeros_like(esn_out[:15])+np.array(esn_out[43:58])*.01)
tmp1.extend(esn_out[58:119])
tmp1.extend(np.zeros_like(esn_out[:20])+np.array(esn_out[99:119])*.01)
tmp1.extend(esn_out[119:])
names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
ln_shape = ["--", ".", "-+", "-", "-.", "-*"]
# import pdb; pdb.set_trace()
tmp1 = np.array(tmp1).transpose()
for idn,g in enumerate(tmp1):
    plt.plot(g*.9,ln_shape[idn],label=names[idn],linewidth=1.0, alpha=0.5)
plt.title("Input Gestures")
plt.xlabel('Time',fontsize=16)
plt.ylabel('Value',fontsize=16)
plt.legend()
plt.tight_layout()
plt.ylim([-1.2,1.2])
plt.savefig("InputGestures.eps")
# plt.show()
# sys.exit()
titles = ["Move Forward", "Turn Left", "Turn Right"]
for idx,gst in enumerate(tmp):
    plt.figure()
    gst = np.array(gst).transpose()
    names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]
    for idn,g in enumerate(gst):
        plt.plot(g*0.9,ln_shape[idn],label=names[idn],linewidth=1.0, alpha=0.5)
    plt.title(titles[idx])
    plt.xlabel('Time',fontsize=16)
    plt.ylabel('Value',fontsize=16)
    # plt.axvline(x=60, alpha=0.3)
    # plt.axvline(x=120, alpha=0.3)
    # plt.axvline(x=180, alpha=0.3)
    # plt.axvspan(90,155, alpha=0.2, color="g")
    # plt.axvline(x=200)
    plt.legend()
    plt.tight_layout()
    plt.ylim([-1.2,1.2])
    plt.savefig(titles[idx]+".eps")

# plt.figure()
# esn_out = user_plot(the_path,"use_in","")
# plt.plot(esn_out)
# plt.show()
