import pickle
import numpy as np
import sys
import csv


def main(argv):

    if len(argv)<2:
        print "input output"
        sys.exit(-1)

    try:
        fout_name = argv[2]
    except Exception as e:
        print argv[1].split(".")
        fout_name = argv[1].split(".")[0]+".csv"
        print "No output name given using, ",fout_name


    fin = open(sys.argv[1],"rb")
    datain= pickle.load(fin)

    fin.close()

    with open(fout_name,"wb") as fout:
        writer = csv.writer(fout, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
        for d in datain:
            writer.writerow(d)


    print "done"


if __name__ == "__main__":
    main(sys.argv)
