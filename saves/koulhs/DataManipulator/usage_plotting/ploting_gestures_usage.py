import pickle
import numpy as np
import os,sys
import matplotlib.pyplot as plt
from matplotlib import rc
from dtw import dtw

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def robot_plot(efile):
    experts_dataset = pickle.load(efile)
    print "Loaded ",len(experts_dataset), " experts."
    return experts_dataset
def user_plot(efile):
    path = efile
    print "Loading file at ", path
    data = pickle.load(open(path,"rb"))
    print "Loaded ", len(data)," entries"
    return data

def dtw_dist(A, B):
    '''
        MD-TDW,
    '''
    return np.sum(np.abs(A-B))
    # return np.sqrt(np.sum(np.square(A-B)))


#load the esn data
# esn_out = user_plot(the_path,"training","IN")
# plt.plot(esn_out)
# plt.show()
# esn_out = user_plot(the_path,"training","OUT")
# plt.plot(esn_out)
# plt.show()

plt.tick_params(axis='x', labelsize=16)
plt.tick_params(axis='y', labelsize=16)

sn_out = user_plot("user_ann_use_out_data.pickle")
sn_in = user_plot("user_ann_use_in_data.pickle")

esn_out = []
esn_in = []
#forward
esn_out.append(sn_out[12800:13300])
esn_in.append(sn_in[12800:13300])
#backward
esn_out.append(sn_out[11925:12100])
esn_in.append(sn_in[11925:12100])
#Left
esn_out.append(sn_out[500:600])
esn_in.append(sn_in[500:600])
#Right
esn_out.append(sn_out[14200:14350])
esn_in.append(sn_in[14200:14350])

forward = sn_in[12800:13300]
bakcward = sn_in[11925:12100]
left = sn_in[500:600]
right = sn_in[14200:14350]
moves = [forward,bakcward,left,right]
#Calculate DTW between forward and backward
dist, cost, path = dtw(sn_in[12800:13300],sn_in[11925:12100],dist = dtw_dist)
print "DTW dist, ",dist

DTW = np.zeros((4,4))
for i in range(4):
    for j in range(4):
        DTW[i,j] = dtw(moves[i], moves[j], dist = dtw_dist)[0]

print "DTW dist, ",DTW
import pdb; pdb.set_trace()

# import pdb; pdb.set_trace()
tites = ["Forward", "Backward","Left", "Right"]
for t,vz in zip(tites,zip(esn_in,esn_out)):
    _in, _out = vz
    f, axarr = plt.subplots(2, sharex=True)

    names = ["Forward", "","","","","Left", "Right"]
    ln_shape = ["--","", "","","","-","-."]
    tmp1 = np.array(_out).transpose()
    for idn,g in enumerate(tmp1):
        axarr[0].plot(g,ln_shape[idn],label=names[idn],linewidth=1., alpha=0.5)
    axarr[0].set_title("Output Activations")
    axarr[0].set_ylabel('Value',fontsize=16)
    axarr[0].legend()
    axarr[0].set_ylim([-1.2,1.2])
    # axarr[0].tight_layout()
    # plt.savefig("InputGestures.eps")
    # plt.show()
    # sys.exit()

    # plt.figure()

    names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]

    tmp1 = np.array(_in).transpose()
    for idn,g in enumerate(tmp1):
        axarr[1].plot(g,ln_shape[idn],label=names[idn],linewidth=1.0, alpha=0.5)
    axarr[1].set_title("Input Gestures")
    axarr[1].set_xlabel('Time',fontsize=16)
    axarr[1].set_ylabel('Value',fontsize=16)
    axarr[1].legend()
    # axarr[1].tight_layout()
    axarr[1].set_ylim([-1.2,1.2])
    # plt.savefig("InputGestures.eps")
    # plt.show()
    # plt.title(t)
    plt.tight_layout()
    plt.savefig(t+"_DoublePlot.eps")
plt.show()

sys.exit()
titles = ["Move Forward", "Turn Left", "Turn Right"]
for idx,gst in enumerate(tmp):
    plt.figure()
    gst = np.array(gst).transpose()
    names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]
    for idn,g in enumerate(gst):
        plt.plot(g,ln_shape[idn],label=names[idn],linewidth=2.0, alpha=0.5)
    plt.title(titles[idx])
    plt.xlabel('Time',fontsize=16)
    plt.ylabel('Value',fontsize=16)
    # plt.axvline(x=60, alpha=0.3)
    # plt.axvline(x=120, alpha=0.3)
    # plt.axvline(x=180, alpha=0.3)
    # plt.axvspan(90,155, alpha=0.2, color="g")
    # plt.axvline(x=200)
    plt.legend()
    plt.tight_layout()
    plt.savefig(titles[idx]+".eps")

# plt.figure()
# esn_out = user_plot(the_path,"use_in","")
# plt.plot(esn_out)
plt.show()
