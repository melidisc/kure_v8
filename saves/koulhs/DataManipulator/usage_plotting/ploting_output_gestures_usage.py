import pickle
import numpy as np
import os,sys
import matplotlib.pyplot as plt
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def robot_plot(efile):
    experts_dataset = pickle.load(efile)
    print "Loaded ",len(experts_dataset), " experts."
    return experts_dataset
def user_plot(efile):
    path = efile
    print "Loading file at ", path
    data = pickle.load(open(path,"rb"))
    print "Loaded ", len(data)," entries"
    return data


#load the esn data
# esn_out = user_plot(the_path,"training","IN")
# plt.plot(esn_out)
# plt.show()
# esn_out = user_plot(the_path,"training","OUT")
# plt.plot(esn_out)
# plt.show()

plt.tick_params(axis='x', labelsize=16)
plt.tick_params(axis='y', labelsize=16)

esn_out = user_plot("user_ann_use_out_data.pickle")
experts = np.zeros((len(esn_out),2))
f=np.array([[1.],[1.]])
r=np.array([[1.],[-1.]])
l=np.array([[-1.],[1.]])

experts = 1/3.*np.array(esn_out)[:,0]*f \
        + 1/3.*np.array(esn_out)[:,5]*l \
        + 1/3.*np.array(esn_out)[:,6]*r

names = ["Forward", "","","","","Left", "Right"]
ln_shape = ["--","", "","","","-","-."]

tmp1 = np.array(esn_out).transpose()
for idn,g in enumerate(tmp1):
    plt.plot(g,ln_shape[idn],label=names[idn],linewidth=.5, alpha=0.5)
plt.title("Input Gestures")
plt.xlabel('Time',fontsize=16)
plt.ylabel('Value',fontsize=16)
plt.legend()
plt.tight_layout()
# plt.savefig("InputGestures.eps")

plt.figure()
experts = experts.transpose()
N=100
p1 = np.convolve(experts[:,0], np.ones((N,))/N, mode='valid')
p2 = np.convolve(experts[:,1], np.ones((N,))/N, mode='valid')
print p1.shape
# plt.plot(p1,label="Left Wheel Velocity")
# plt.plot(p2,label="Right Wheel Velocity")
plt.xlabel("Time")
plt.ylabel("Value")
plt.tight_layout()

p = np.column_stack((p1,p2))

behaviours = []
#steep
behaviours.extend(p[200:530])
#less steep
behaviours.extend(p[1220:1500])
#forward
behaviours.extend(p[4050:4500])
#modulation
behaviours.extend(p[5200:5550])
#stopping
behaviours.extend(p[7900:8250])
#less steep
behaviours.extend(p[11600:12300])

plt.plot(behaviours)


plt.show()
sys.exit()


titles = ["Move Forward", "Turn Left", "Turn Right"]
for idx,gst in enumerate(tmp):
    plt.figure()
    gst = np.array(gst).transpose()
    names = ["surging", "heaving", "swaying",  "rolling", "pitching", "yawing"]
    ln_shape = ["--", ".", "-+", "-", "-.", "-*"]
    for idn,g in enumerate(gst):
        plt.plot(g,ln_shape[idn],label=names[idn],linewidth=2.0, alpha=0.5)
    plt.title(titles[idx])
    plt.xlabel('Time',fontsize=16)
    plt.ylabel('Value',fontsize=16)
    # plt.axvline(x=60, alpha=0.3)
    # plt.axvline(x=120, alpha=0.3)
    # plt.axvline(x=180, alpha=0.3)
    # plt.axvspan(90,155, alpha=0.2, color="g")
    # plt.axvline(x=200)
    plt.legend()
    plt.tight_layout()
    plt.savefig(titles[idx]+".eps")

# plt.figure()
# esn_out = user_plot(the_path,"use_in","")
# plt.plot(esn_out)
plt.show()
