import pickle
import numpy as np
import os,sys
import matplotlib.pyplot as plt
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

def robot_plot(efile):
    experts_dataset = pickle.load(efile)
    print "Loaded ",len(experts_dataset), " experts."
    return experts_dataset
def user_plot(efile,what,where):
    path = efile+"/user_ann/user_ann_"+what+"_data"+where+".pickle"
    print "Loading file at ", path
    data = pickle.load(open(path,"rb"))
    print "Loaded ", len(data)," entries"
    return data

kure_path =  os.path.split(os.path.abspath(__file__))[0][:-6]
folder_name = sys.argv[1]
the_path = kure_path+"/saves/"+folder_name

if not os.path.exists(the_path):
    print "Wrong folder name", folder_name
    sys.exit(0)

print "Loading ", the_path

#load the esn data
# esn_out = user_plot(the_path,"training","IN")
# plt.plot(esn_out)
# plt.show()
# esn_out = user_plot(the_path,"training","OUT")
# plt.plot(esn_out)
# plt.show()

esn_out = user_plot(the_path,"use_out","")

tmp = esn_out[:90]
tmp.extend(esn_out[120:210])
tmp.extend(esn_out[350:600])

I1 = np.array(tmp)
print I1.shape
# plt.plot(esn_out)
# plt.show()
plt.figure()
plt.tick_params(axis='x', labelsize=16)
plt.tick_params(axis='y', labelsize=16)
for idx in range(3):
 	column = I1[:,idx]
 	if (idx==0):
		plt.plot(column,"--",label="CW "+str(idx+1),linewidth=2.0, alpha=0.5)
	elif (idx==1):
		plt.plot(column,"+",label="ACW "+str(idx+1),linewidth=2.0, alpha=0.5)
	else:
		plt.plot(column,label="Up-Down "+str(idx+1))
	plt.title('Echo State Network Output')
	plt.xlabel('Time',fontsize=16)
	plt.ylabel('Value',fontsize=16)
plt.axvline(x=90, alpha=0.3)
plt.axvline(x=155, alpha=0.3)
plt.axvline(x=180, alpha=0.3)
# plt.axvspan(90,155, alpha=0.2, color="g")
# plt.axvline(x=200)
plt.legend()

# plt.figure()
# esn_out = user_plot(the_path,"use_in","")
# plt.plot(esn_out)
plt.show()
