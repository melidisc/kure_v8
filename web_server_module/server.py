#server
from gevent import monkey
monkey.patch_all()


import flask
from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit

#engine
from input_control_module.esn.serv_ESN import serv_ESN
from interface_module.interface import Kure
from robot_control_module.controller import Controller

#os
from datetime import datetime
import os, sys
import numpy as np
from time import sleep,time
from threading import Thread
import subprocess


class Serverakla():
    def __init__(self,folderName=None):
        self.app = Flask(__name__)
        self.app.config.from_pyfile('config.py')
        self.ws = SocketIO(self.app)

        f = os.path.split(os.path.abspath(__file__)[:-10])[0]
        print "KURE path: ",f
        print "Saving folder ", f+"/saves/"+folderName

        print "Starting the Input Control module ..."
        self.user_ann = serv_ESN()
        print "Starting the Robot Control module ..."
        self.robot_ann = Controller(f)#"sphere_walker")
        print "Starting the Networking module ..."
        self.interface = Kure()

        if folderName!=None:
            # import pdb; pdb.set_trace()

            self.robot_ann.folderName = f+"/saves/"+folderName+"/robot_ann/"
            self.user_ann.folderName = f+"/saves/"+folderName+"/user_ann/"

        @self.app.route('/touchscreen')
        @self.app.route('/leap')
        @self.app.route('/')
        def main():
            return render_template('index.html')

        self.ws.on(
            'setup_input', namespace='/control')(
            self.user_ann.setup_device)
        self.ws.on(
            'setup_robot', namespace='/control')(
            self.robot_ann.setup_robot)

        self.ws.on(
            'input_control', namespace='/control')(
            self.interface.input_front_end_control)
        self.ws.on(
            'input_control', namespace='/control_background')(
            self.user_ann.back_end_control)

        self.ws.on(
            'robot_control', namespace='/control')(
            self.interface.robot_front_end_control)
        self.ws.on(
            'robot_control', namespace='/control_background')(
            self.robot_ann.back_end_control)

        # self.ws.on('interface', namespace='/control')(interface.interface_front_end_control)
        self.ws.on(
            'interface', namespace='/control_background')(
            self.interface.back_end_control)

        '''
        {
        action: ,
        source: ,
        destination: ,
        value:
        {exp_ID}
        }

        '''
    def start(self):

        port = int(os.environ.get("PORT", 3001))
        print "Starting the Server at port :", 3001
        self.ws.run(self.app, host='0.0.0.0', port=port)

    def close(self,):
        self.user_ann.serv_close()
        self.robot_ann.close()
        self.interface.close()

if __name__ == '__main__':
    print "Init launch sequence"

    if len(sys.argv)>1:
        server = Serverakla(sys.argv[1])
    else:
        server = Serverakla()
    try:
        server.start()
    except KeyboardInterrupt as e:
        print "It was writen ",e
        server.close()

    sys.exit(1)
