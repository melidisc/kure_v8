'use strict';

angular.module('webApp')
    .controller('MainServerCtrl', function ($window,$rootScope, $scope, socket, socket_test) {
      console.log("MainServerCtrl");
      $scope.switch_1 = false;
      $scope.switch_2 = false;
      //.btn.disabled
      $scope.device_s = -1;
      $scope.robot_s = -1;

      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      //messages comming from the server
      //make them alerts
      socket.forward('notification', $rootScope);
      $rootScope.$on('socket:notification', function (ev, data) {
          console.log(data);
          toastr.info(data);
          //we can put sync here to change all together
      });

      $scope.devices = [
        {name:"Leap", arg:0,path:"/leap"},
        {name:"TouchScreen", arg:1, path:"/touchscreen"}];
      $scope.robots = [
          {name:"Acrobot", arg:"acrobot"},
          {name:"Snake",arg:"ccrl"},
          {name:"SnakeS",arg:"ccrl_stif"},
          {name:"Octacrawl", arg:"octacrawl"},
          {name:"Sphere Walker", arg:"sphere_walker"},
          {name:"Arm", arg:"arm"},
          {name:"Ball", arg:"ball"}];

      var change_switches = function(){
        $rootScope.switch_1  = $scope.switch_1;
        $rootScope.switch_2  = $scope.switch_2;
        if ($scope.switch_1 && $scope.switch_2){
            redirect();
        }
      }

      var redirect = function(){
        // var device = $.grep($scope.devices, function(e){ return e.arg == $scope.device_s; })[0];
        toastr.info("Relocating to "+$scope.device_s.path);
        // if ($scope.device_s.arg == 1){
        //   var myElement = document.getElementById("touchscreen");
        //   myElement.style.marginTop = "0px";
        //   myElement.style.marginLeft = "0px";
        //   myElement.style.paddingLeft = "0px";
        //   myElement.style.paddingTop = "0px";
        //   $scope.$on('$routeChangeSuccess', function() {
        //     //call it here
        //     var myElement = document.getElementById("touchscreen");
        //     myElement.style.marginTop = "0px";
        //     myElement.style.marginLeft = "0px";
        //     myElement.style.paddingLeft = "0px";
        //     myElement.style.paddingTop = "0px";
        //     console.log(myElement.style);
        //
        //   });
        // }
        $window.location.href = "/#"+$scope.device_s.path;
      }

      $scope.deviceSelect = function(arg){
          $scope.device_s = arg;
      }
      $scope.robotSelect = function(arg){
          $scope.robot_s = arg;
      }
      $scope.ann_switch = function(){
        console.log($scope.device_s);
        if ($scope.device_s != -1)
        {
          $scope.switch_1 = !$scope.switch_1;
          socket.emit('setup_input',{
                    switch:$scope.switch_1,
                    device:$scope.device_s.arg
                  }
            );
            toastr.info("Initialising "+$scope.device_s.name+" input device");
            console.log("emiting, input_control");
            change_switches();
        }
      }
      $scope.music_switch = function(){
        if ($scope.robot_s != -1)
        {
          $scope.switch_2 = !$scope.switch_2;
          socket.emit('setup_robot',{
                switch:$scope.switch_2,
                device:$scope.robot_s.arg}
              );
          toastr.info("Initialising "+$scope.robot_s.name+" morphology");
          console.log("emiting, robot_control");
          change_switches();
        }
      }
    });
