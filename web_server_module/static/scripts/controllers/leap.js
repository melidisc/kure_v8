'use strict';

angular.module('webApp')
.controller('RobotServerCtrl', function ($window, $scope, socket, socket_test) {
    //notifications manage
    console.log('RobotServerCtrl');
    console.log("Input net on:",$scope.switch_1);
    console.log("Robot net on:",$scope.switch_2);
    var canvasElement = document.getElementById("displayArea");
    canvasElement.width = document.getElementById("drawable").clientWidth;
    var displayArea = canvasElement.getContext("2d");
    if (!($scope.switch_1 && $scope.switch_2)){
        $window.location.href = "/#/";
        toastr.info("Relocating to main...");
      }



    $scope.ann_btn = "default";


    $scope.step1learning_on= false;
    $scope.step2learning_on= false;
    $scope.step3learning_on= false;

    $scope.expertsON=[];

    $scope.sensorsList = [];
    $scope.leapAVG = 5;
    $scope.leapCOUNT = $scope.leapAVG;
    $scope.wash_counter_val = 5;
    $scope.wash_counter = $scope.wash_counter_val;
    // //messages comming from the server
    // //make them alerts
    // socket.forward('notification', $scope);
    // $scope.$on('socket:notification', function (ev, data) {
    //     console.log(data);
    //     toastr.info(data);
    //     //we can put sync here to change all together
    // });

    socket_test.forward('input_control', $scope);
    socket_test.forward('robot_control', $scope);
    socket_test.forward('interface', $scope);

    $scope.$on('socket:input_control', function (ev, data) {
        // console.log("input_control",data);
        data.source = "front";
        socket_test.emit(data.destination,data);
        // console.log(data.destination,data);
        //we can put sync here to change all together
    });
    $scope.$on('socket:robot_control', function (ev, data) {
        // console.log("robot_control",data);
        if (data.action=="experts_trained"){
            // console.log(data);
            $scope.expertsON=[];
            for (var i = 0; i < data.value.length; i++) {
                var v = data.value[i];
                $scope.expertsON.push({
                    id: v,
                    on:false
                })
            }
            // console.log($scope.expertsON);
        }
        if (
            (data.action=="activate" || data.action=="reset") &&
             data.source=="input_control"){
            socket_test.emit(data.destination,data);
            return 0;
        }

        data.source = "front";
        if (data.destination != "front")
            socket_test.emit(data.destination,data);

    });
    $scope.$on('socket:interface', function (ev, data) {
        console.log(data);
        // data.source = "front";
        // socket_test.emit(data.destination,data);

    });

    // $scope.packet = {
    //     action: "none",
    //     source: "front",
    //     destination: "none",
    //     value: 0
    // };
    $scope.step1learning = function(){
        //its on, clicking makes it off, stopping sml
        if ($scope.step1learning_on){
               socket_test.emit('robot_control',{
                action:"train",
                source:"front",
                destination:"robot_control",
                value:false
            });
        }
        else{
            // starting sml
            socket_test.emit('robot_control',{
                action:"train",
                source:"front",
                destination:"robot_control",
                value:true
            });
        }
        $scope.step1learning_on = !$scope.step1learning_on;

    }
    $scope.step2learning_reset = function(){
        socket_test.emit('input_control',{
            action:"reset",
            source:"front",
            destination:"input_control",
            value:true
        });
    }
    $scope.step2learning = function(){
        // stopping experts show off
        if ($scope.step2learning_on){
            socket_test.emit('robot_control',{
                action:"step",
                source:"front",
                destination:"robot_control",
                value:false
            });
            socket_test.emit('input_control',{
                action:"training_values",
                source:"front",
                destination:"input_control",
                value:false
            });

        }
        else{
            // starting experts show off
            socket_test.emit('robot_control',{
                action:"step",
                source:"front",
                destination:"robot_control",
                value:[1]
            });
        }
        $scope.step2learning_on = !$scope.step2learning_on;
    }
    $scope.expSwitch = function(id){
        //change the value

        for (var i=0; i< $scope.expertsON.length; i++)
        {
            if ($scope.expertsON[i].id == id){
                $scope.expertsON[i].on = !$scope.expertsON[i].on;
                // if ($scope.expertsON[i].on)
                // {
                //     socket_test.emit('robot_control',{
                //     action:"reset",
                //     source:"input_control",
                //     destination:"robot_control",
                //     value:true
                //     });
                // }
              }
        }
        // $scope.sendExpertsToActivate();

    }
    $scope.sendExpertsToActivate = function(){
        var exps = [];
        for (var i=0; i< $scope.expertsON.length; i++)
        {
            if ($scope.expertsON[i].on)
                exps.push($scope.expertsON[i].id);
        }
        if (exps.length == 0)
          exps = -1;

        // if (expert.on){
            socket_test.emit('robot_control',{
                    action:"activate",
                    source:"front",
                    destination:"robot_control",
                    value:exps
                });

        // }
        // }
    }

    $scope.step3learning = function(){
        $scope.step3learning_on = !$scope.step3learning_on;
        if (!$scope.step3learning_on){
            socket_test.emit('input_control',{
                    action:"step",
                    source:"front",
                    destination:"input_control",
                    value:false
                });
        }
    }

    var controllerOptions = {enableGestures: false};
    $scope.sensor_values_old={
        x:0.,
        y:0.,
        z:0.,
        roll:0.,
        pitch:0.,
        yaw:0.
    };
    var controller = new Leap.Controller();
    controller.connect();
    controller.on('frame', onFrame);


    // Leap.loop(controllerOptions,
    function onFrame(frame){
            $scope.fps = frame.currentFrameRate;
            if (($scope.step2learning_on || $scope.step3learning_on) && frame.hands.length == 0 )
              {
                if($scope.wash_counter == 0)
                {
                  console.log("IN HERE ",$scope.wash_counter);
                  $scope.wash_counter = $scope.wash_counter_val;
                  $scope.sendExpertsToActivate();
                  if ($scope.step3learning_on ){
                    socket_test.emit('input_control',{
                        action:"wash_out",
                        source:"front",
                        destination:"input_control",
                        value:true

                    });
                  }
                }
                else{
                  $scope.wash_counter -= 1;
                }
              }

            if (frame.hands.length > 0 && ($scope.step2learning_on || $scope.step3learning_on)) {
                var pointable = frame.hands;
                var interactionBox = frame.interactionBox;

                for (var i = 0; i < frame.hands.length; i++) {
                    var hand = frame.hands[i];
                    var normalizedPosition = interactionBox.normalizePoint(hand.palmPosition, true);
                    canvasElement.width = canvasElement.width;
                    //  //clear

                    // Convert the normalized coordinates to span the canvas
                    var canvasX = canvasElement.width * normalizedPosition[0];
                    var canvasY = canvasElement.height * (1 - normalizedPosition[1]);
                    //we can ignore z for a 2D context
                    displayArea.strokeText($scope.fps, canvasX, canvasY);


                    var sensor_values={
                        x:((normalizedPosition[0] *2.)-1.),
                        y:((normalizedPosition[1] *2.)-1.),
                        z: ((normalizedPosition[2] *2.)-1.),
                        roll:hand.roll()/Math.PI,
                        pitch:hand.pitch()/Math.PI,
                        yaw: hand.yaw()/Math.PI
                    };
                    // console.log((normalizedPosition[0] *2.)-1. -($scope.sensor_values_old.x), (normalizedPosition[0])-$scope.sensor_values_old.x);
                    // console.log("direction",hand.direction[0]);
                    var sensor_values_raw=[
                       ((normalizedPosition[0] *2.)-1.),
                       ((normalizedPosition[1] *2.)-1.),
                       ((normalizedPosition[2] *2.)-1.),
                       hand.roll()/Math.PI,
                       hand.pitch()/Math.PI,
                       hand.yaw()/Math.PI
                    ];
                    // var sensor_values_send = {
                    //    x:sensor_values.x - $scope.sensor_values_old.x,
                    //   y:sensor_values.y - $scope.sensor_values_old.y,
                    //   z:sensor_values.z - $scope.sensor_values_old.z,
                    //   roll:sensor_values.roll- $scope.sensor_values_old.roll,
                    //   pitch:sensor_values.pitch- $scope.sensor_values_old.pitch,
                    //   yaw:sensor_values.yaw- $scope.sensor_values_old.yaw
                    // };
                    var sensor_values_send = sensor_values;

                    $scope.sensor_values_old = sensor_values;

                    $scope.leapCOUNT -=1;
                    $scope.sensorsList.push(sensor_values_raw);
                    if ($scope.step2learning_on ){
                        if ($scope.leapCOUNT == 0){
                            $scope.sendExpertsToActivate();
                            socket_test.emit('input_control',{
                                action:"training_values",
                                source:"front",
                                destination:"input_control",
                                value:{
                                    exps:$scope.expertsON,
                                    inputVal:sensor_values_send}

                            });
                            $scope.leapCOUNT = $scope.leapAVG;
                            $scope.sensorsList = [];
                        }
                    }
                    else{
                        if ($scope.leapCOUNT == 0){
                            socket_test.emit('input_control',{
                                action:"step",
                                source:"front",
                                destination:"input_control",
                                value:sensor_values_send
                            });
                            $scope.leapCOUNT = $scope.leapAVG;
                            $scope.sensorsList = [];
                        }
                    }


                }
            }
        }


});
