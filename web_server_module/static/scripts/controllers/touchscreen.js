'use strict';


angular.module('webApp')
.controller('TouchScreenCtrl', function ($route,$window, $scope, socket, socket_test) {
  //angular scope vars
  if (!($scope.switch_1 && $scope.switch_2)){
    $window.location.href = "/#/";
    toastr.info("Relocating to main...");
  }

  $scope.ann_btn = "default";


  $scope.step1learning_on= false;
  $scope.step2learning_on= false;
  $scope.step3learning_on= false;

  $scope.expertsON=[];

  $scope.sensorsList = [];
  $scope.leapAVG = 5;
  $scope.leapCOUNT = $scope.leapAVG;
  $scope.wash_counter_val = 30;
  $scope.wash_counter = $scope.wash_counter_val;


  socket_test.forward('input_control', $scope);
  socket_test.forward('robot_control', $scope);
  socket_test.forward('interface', $scope);

  $scope.$on('socket:input_control', function (ev, data) {
      // console.log("input_control",data);
      data.source = "front";
      socket_test.emit(data.destination,data);
      // console.log(data.destination,data);
      //we can put sync here to change all together
  });
  $scope.$on('socket:robot_control', function (ev, data) {
      // console.log("robot_control",data);
      if (data.action=="experts_trained"){
          // console.log(data);
          $scope.expertsON=[];
          for (var i = 0; i < data.value.length; i++) {
              var v = data.value[i];
              $scope.expertsON.push({
                  id: v,
                  on:false
              })
          }
          // console.log($scope.expertsON);
      }
      if (
          (data.action=="activate" || data.action=="reset") &&
           data.source=="input_control"){
          socket_test.emit(data.destination,data);
          return 0;
      }

      data.source = "front";
      if (data.destination != "front")
          socket_test.emit(data.destination,data);

  });
  $scope.$on('socket:interface', function (ev, data) {
      console.log(data);
      // data.source = "front";
      // socket_test.emit(data.destination,data);

  });
  //$("#wrapper").toggleClass("toggled");
  $scope.step1learning = function(){
      //its on, clicking makes it off, stopping sml
      if ($scope.step1learning_on){
             socket_test.emit('robot_control',{
              action:"train",
              source:"front",
              destination:"robot_control",
              value:false
          });
      }
      else{
          // starting sml
          socket_test.emit('robot_control',{
              action:"train",
              source:"front",
              destination:"robot_control",
              value:true
          });
      }
      $scope.step1learning_on = !$scope.step1learning_on;

  }
  $scope.step2learning_reset = function(){
      socket_test.emit('input_control',{
          action:"reset",
          source:"front",
          destination:"input_control",
          value:true
      });
  }
  $scope.step2learning = function(){
      // stopping experts show off
      if ($scope.step2learning_on){
          socket_test.emit('robot_control',{
              action:"step",
              source:"front",
              destination:"robot_control",
              value:false
          });
          socket_test.emit('input_control',{
              action:"training_values",
              source:"front",
              destination:"input_control",
              value:false
          });

      }
      else{
          // starting experts show off
          socket_test.emit('robot_control',{
              action:"step",
              source:"front",
              destination:"robot_control",
              value:[1]
          });
      }
      $scope.step2learning_on = !$scope.step2learning_on;
      if ($scope.step2learning_on){
        $scope.asd.start();
      }
  }
  $scope.expSwitch = function(id){
      //change the value

      for (var i=0; i< $scope.expertsON.length; i++)
      {
          if ($scope.expertsON[i].id == id){
              $scope.expertsON[i].on = !$scope.expertsON[i].on;
              // if ($scope.expertsON[i].on)
              // {
              //     socket_test.emit('robot_control',{
              //     action:"reset",
              //     source:"input_control",
              //     destination:"robot_control",
              //     value:true
              //     });
              // }
            }
      }
      // $scope.sendExpertsToActivate();

  }
  $scope.sendExpertsToActivate = function(){
      var exps = [];
      for (var i=0; i< $scope.expertsON.length; i++)
      {
          if ($scope.expertsON[i].on)
              exps.push($scope.expertsON[i].id);
      }
      if (exps.length == 0)
        exps = -1;

      // if (expert.on){
          socket_test.emit('robot_control',{
                  action:"activate",
                  source:"front",
                  destination:"robot_control",
                  value:exps
              });

      // }
      // }
  }

  $scope.step3learning = function(){
      $scope.step3learning_on = !$scope.step3learning_on;
      if (!$scope.step3learning_on){
          socket_test.emit('input_control',{
                  action:"step",
                  source:"front",
                  destination:"input_control",
                  value:false
              });
      }
      else{
        $scope.asd.start();
      }
  }

  var controllerOptions = {enableGestures: false};
  $scope.sensor_values_old={
      x:0.,
      y:0.,
      z:0.,
      roll:0.,
      pitch:0.,
      yaw:0.
  };

  $scope.moves=[0,0];
  $scope.play = false;
  //js global vars
  var COLOURS = [ '#E3EB64', '#A7EBCA', '#FFFFFF', '#D8EBA7', '#868E80' ];
  //stroke radius
  var radius = 0;
  //no idea
  var i=0;
  //force UI to update values to
  //the server only every 5 updates
  var update_slow = 5;
  // $scope.$watch('behaviour',function(old, newv){
  //   $scope.inited = "No";
  // });
  // $scope.erase = function(){
  //   $scope.asd.clear();
  //   $scope.record = true;
  //   $socketio.emit("erase", "");
  // };
  // var canvasElement = document.getElementById("container");
  // canvasElement.width = document.getElementById("touchscreen").clientWidth-30;//$window.innerHeight;//
  // canvasElement.height = document.getElementById("touchscreen").clientHeight-50;//$window.innerWidth;//
  // console.log(canvasElement);
  $scope.asd = Sketch.create({
    container: document.getElementById( 'container' ),
    autoclear: false,
    retina:false,
    fullscreen: false,
    width: $window.innerWidth-20, height: $window.innerHeight-95,
    autostart:false,
    // background_color:"2b2b2b",
    setup: function() {
      console.log( 'setup' );

    },
    update: function() {
      $scope.coor_old = $scope.coor;
      $scope.coor= [];
      for ( var i = this.touches.length - 1, touch; i >= 0; i-- ) {
        touch = this.touches[i];
        var sensor_values_send={
          // 'ox':touch.ox, 'oy':touch.oy,
          x:(Math.max(touch.x/$window.innerWidth,0.)*2.)-1.,
          y:(Math.max(touch.y/$window.innerHeight,0)*2.)-1.,
          // 'dx':touch.dx, 'dy': touch.dy,
        };
        // console.log(sensor_values_send);
        $scope.leapCOUNT -=1;
        if ($scope.step2learning_on ){
            if ($scope.leapCOUNT == 0){
                console.log("emiting step2");
                $scope.sendExpertsToActivate();
                socket_test.emit('input_control',{
                    action:"training_values",
                    source:"front",
                    destination:"input_control",
                    value:{
                        exps:$scope.expertsON,
                        inputVal:sensor_values_send}

                });
                $scope.leapCOUNT = $scope.leapAVG;
                $scope.sensorsList = [];
            }
        }
        else if($scope.step3learning_on ){
            if ($scope.leapCOUNT == 0){
                console.log("emiting step3");
                socket_test.emit('input_control',{
                    action:"step",
                    source:"front",
                    destination:"input_control",
                    value:sensor_values_send
                });
                $scope.leapCOUNT = $scope.leapAVG;
                $scope.sensorsList = [];
            }
        }
        //calculate a velocity measure as the radious measure
        radius = min(40,sqrt(pow(touch.dx,2) + pow(touch.dy,2)));

      }
    },
    // Mouse & touch events are merged, so handling touch events by default
    // and powering sketches using the touches array is recommended for easy
    // scalability. If you only need to handle the mouse / desktop browsers,
    // use the 0th touch element and you get wider device support for free.
    touchmove: function() {
      for ( var i = this.touches.length - 1, touch; i >= 0; i-- ) {
        touch = this.touches[i];
        this.fillStyle = this.strokeStyle = COLOURS[ i % COLOURS.length ];
        this.lineWidth = radius;
        //keep a reference to shrink
        this.lineCap = 'round';
        this.lineJoin = 'round';
        this.beginPath();
        this.moveTo( touch.ox, touch.oy );
        this.lineTo( touch.x, touch.y );
        this.stroke();
      }
    },
    touchend:function(){
      this.clear();
      $scope.sk_width = this.container.clientWidth;
      $scope.sk_height = this.container.clientHeight;
      console.log(this.container.clientWidth,this.container.clientHeight);
    }
  })

});
