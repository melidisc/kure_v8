'use strict';

angular.module('webApp', [
  'ngAnimate',
  'btford.socket-io',
  // 'angularMoment',
  // 'luegg.directives',
  'ngRoute'
])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/touchscreen', {
      templateUrl: 'static/partials/touchscreen.html',
      controller: 'TouchScreenCtrl'
    })
    .when('/leap', {
      templateUrl: 'static/partials/leap.html',
      controller: 'RobotServerCtrl'
    })
    .when('/', {
      templateUrl: 'static/partials/landing.html',
      controller: 'MainServerCtrl'
    })
    .otherwise({redirectTo: '/'});
}]);
