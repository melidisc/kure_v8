from input_control_module.controller import Controller as u_ctrl
from robot_control_module.controller import Controller as r_ctrl
import logging
logging.basicConfig()#level=logging.INFO
import flask
from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit

class Kure:
	def __init__(self, input_controller=None, robot_controller=None, server=None):
		return None
		self.user = input_controller
		self.robot = robot_controller
		self.server = server

	def set_robot_control(self, controller):
		self.robot = controller

	def set_user_control(self, controller):
		self.user = controller

	def train(self,):
		self.robot.train()
		self.user.train()

	def use(self,):

		self.robot.step(
			self.user.step()
			)
	def close(self,):
		return True

	def input_front_end_control(self,data):

		if data:
			#switch off
			data['source'] = 'interface'

			emit(
				'input_control',
				data,
				namespace='/control_background',
				broadcast=True)

			emit(
				'notification',
				"INPUT switch "+str(data['value'])+".",
				namespace='/control',
				broadcast=True
				)

			logging.info("INPUT::switch "+str(data['value']))

	def robot_front_end_control(self,data):
		if data:
			#switch off
			data['source'] = 'interface'

			emit(
				'robot_control',
				data,
				namespace='/control_background',
				broadcast=True)

			emit(
				'notification',
				"INPUT "+ str(data['action']) +" " +str(data['value'])+".",
				namespace='/control',
				broadcast=True
				)

			logging.info("ROBOT::switch "+str(data['value']))

	def back_end_control(self,data):
		#getting input from the controlling device
		if data:
			#we have the leap motion data
			# print data
			pass
		else:
			logging.info(
				"INTERFACE::back_end_control no data received"
				)



if __name__=="__main__":
	# server = Server()
	# server.start()
	print "nothing to see here"
