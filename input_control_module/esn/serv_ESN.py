import numpy as np
import matplotlib.pyplot as plt
from esn import ESN
from tools import read_dataset, make_train_set,sig,createPad,Sine
import sys
from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit
import os
import logging
logging.basicConfig(level=logging.INFO)
import pickle

class serv_ESN(object):
	def __init__(self):
		self.initialised = False

	def initialise(self, indim=6, w_inp_s=.9, units = None, fback = None, weight_fb = None, alpha=None):
		self.initialised = True

		self.weight_scale = .3#.6#.4#1.#.8
		self.weight_inp = w_inp_s#.9#.9
		self.weight_fb = weight_fb or 10**(-2)#-2
		self.alpha = alpha or .995#.8#.35#.2
		self.fback = fback or True#True#True#False#False
		self.inital_washout = 20#100
		self.padding_s = 50
		self.units = units or 2048#1024#13*13#28*28#
		self.indim = indim
		self.outdim = 7

		self.esn = ESN(
			self.units, self.indim, self.outdim,
			self.weight_scale,
			self.weight_inp,
			self.weight_fb,
			self.alpha,
			self.fback
			)

		# self.webapp = webapp
		# local_dir = os.path.dirname(__file__)
		# self.esn.load(local_dir+"/trainied.pickle")
		self.stepper = self.esn.step_taped()
		self.step = self.esn.step()
		self.trainer = self.esn.train()

		#list of lists for training
		self.train_outs = []
		self.train_inputs = []

		#temporary lists for training
		self.tmp_outs = []
		self.tmp_inputs = []

		self.expertsList = []
		self.rollingInputs_size = 3
		# self.idx= 0
		self.outputs = np.zeros((self.rollingInputs_size,self.outdim))
		self.rollingInputs = np.zeros((self.rollingInputs_size,self.indim)).tolist()
		self.state = np.zeros(self.units+self.indim+self.outdim)
		#[[np.zeros(self.indim)] for _ in range(self.rollingInputs_size)]

		self.output_window = 4
		self.output_sliding_window = np.zeros(
			(self.output_window,self.outdim)).tolist()

		self.all_states = [np.zeros(self.units)]
		self.all_inputs = [np.zeros(self.indim)]
		self.all_outputs = [np.zeros(self.outdim)]
		self.once = False
		# self.folderName = None

		self.resting_pos = np.zeros(self.indim)
		self.resting_pos_bucket = []
		self.resting_pos_set = False

		self.sine = Sine()
		print "ESN:: init"
		emit(
			'notification',
			"Echo State Network initialised",
			namespace='/control',
			broadcast=False
			)
	def setup_device(self,data):
		if self.initialised:
			self.serv_close()
		if data['switch']:
			self.device = data['device']
			if self.device == 1:
				self.initialise(
					indim = 2,
					w_inp_s = 0.9,
					units= 100,
					weight_fb = 10**(-5),
					alpha= 0.3)
			else:
				self.initialise(fback= True)

	def init_live_graph(self,):
		self.once = False
		self.fig = plt.figure()
		plt.ion()
		plt.show(False)
		plt.axis([0, 400, -1, 1])
		ax1 = self.fig.add_subplot(3, 1, 1)
		ax2 = self.fig.add_subplot(3, 1, 2)
		ax3 = self.fig.add_subplot(3, 1, 3)

		# import pdb;pdb.set_trace()
		ax1.set_title('Network Output')
		ax1.set_xlabel('Time')
		ax1.set_ylabel('Values')
		ax2.set_title('Network State')
		ax2.set_xlabel('Time')
		ax2.set_ylabel('Values')
		ax3.set_title('Network Inputs')
		ax3.set_xlabel('Time')
		ax3.set_ylabel('Values')
		# axarr[0].legend()
		# self.line1 = ax1.plot(self.all_outputs,label="output")
		for v in self.all_outputs[0]:
			self.line1, = ax1.plot(v,label="output")
		for v in self.all_states[0]:
			self.line2, = ax1.plot(v,label="states")
		for v in self.all_inputs[0]:
			self.line3, = ax1.plot(v,label="input")
		# self.line2 = ax2.plot(self.all_states,label="state")
		# self.line3 = ax3.plot(self.all_inputs,label="inputs")

		self.fig.show()
		self.fig.canvas.draw()
		# axarr[1].legend()
		# pdb.set_trace()

	def serv_close(self,):
		del self.esn
		del self.stepper
		del self.trainer
		return True

	def serv_train(self, value):
		'''
		make an acumulator and a test set
		we are getting
	 	value:{
            exps:$scope.expertsON,
            inputVal:sensor_values
        }
		'''
		def getExpertsOn(exps):
			expsList = []#range(self.outdim)
			for ex in exps:
				if ex['on']:
					expsList.append(ex['id'])
			return expsList
		def makeActiveExpsArray(expsList):
			out = np.zeros(self.outdim)
			for num in expsList:
				out[num] = .1
			return out
		def getInputValues(dicitionary):
			return dicitionary.values()
		#get experts to be active with the inputs
		expsList = getExpertsOn(value['exps'])
		vals = getInputValues(value['inputVal'])

		if expsList == []:
			# self.resting_pos_bucket.append(vals)
			# if len(self.resting_pos_bucket) > 10:
			# 	self.resting_pos_bucket = self.resting_pos_bucket[1:]
			# self.resting_pos = np.mean(self.resting_pos_bucket,axis=0)
			return False



		# 	#add resting position padding
		# 	self.train_outs.append(
		# 		makeActiveExpsArray([])
		# 		)
		# 	return True

		'''
		if expsList != self.expertsList:
			if self.tmp_outs != [] and self.tmp_inputs!= []:
				self.train_outs.append(self.tmp_outs)
				self.train_inputs.append(self.tmp_inputs)
			self.tmp_outs = []
			self.tmp_inputs = []
			self.expertsList = expsList

		self.tmp_outs.append(
			makeActiveExpsArray(expsList)
			)
		vals = getInputValues(value['inputVal'])
		self.tmp_inputs.append(
			vals
			)
		'''

		mse = np.mean((np.array(self.resting_pos) - np.array(vals))**2)

		if mse < 0.001 and not self.resting_pos_set:
			self.resting_pos_set = not self.resting_pos_set
			emit(
				'notification',
				"Resting position set",
				namespace='/control',
				broadcast=False
				)

		print "MSE ",mse," rp ",self.resting_pos, " vals ",vals

		#save them as training samples only if they are different enough
		if mse > -0.05:
			# self.resting_pos_set = not self.resting_pos_set
			self.train_outs.append(
				makeActiveExpsArray(expsList)
				)
			try:
				self.train_inputs = self.train_inputs.tolist()
			except Exception as e:
				logging.info("Exception caught "+ str(e))
			self.train_inputs.append(
				vals
				)

		return True
	def train(self,):
		all_states = []
		all_this = []
		all_inputs = []
		all_outputs = []
		if self.train_inputs!=[] and self.train_outs!=[]:
			trainp = []
			trainv = []
			tmppi = []
			tmppo = []
			tmpvi = []
			tmpvo = []
			tmp_old_out = self.train_outs[0]
			for inp,out in zip(self.train_inputs,self.train_outs):

				# if all(out == -.1):
					#this is padding
					# tmppi.append(inp)
					# tmppo.append(out)
				# else:
					#this is values
				tmpvi.append(inp)
				tmpvo.append(out)

				if all(out == tmp_old_out):
					#still in the same bucket
					pass
				else:
					#new value new bucket
					if all(out == 0.):
						trainv.append([tmpvi,tmpvo])
						tmpvi = []
						tmpvo = []
					else:
						trainp.append([tmpvi,tmpvo])
						tmpvi = []
						tmpvo = []
						# trainp.append([tmppi,tmppo])
						# tmppi = []
						# tmppo = []
				tmp_old_out = out
			# 	# create a random pad
			# 	padIn = 0.1*createPad(100,3,self.indim)
			# 	padOut = 0.1*createPad(100,3,self.outdim)
			# 	# pass it through the net
			# 	# import pdb;pdb.set_trace()
			# 	_s,_o,_t = self.stepper(
			# 			padIn,
			# 			np.array(out),
			# 			.0)
			# 	all_states.extend(_s)
			# 	all_this.extend(_t)
			# 	# pass the values to train
			# 	state, _output, this = self.stepper(
			# 			np.array(inp),
			# 			np.array(out),
			# 			1.)
			# 	all_states.extend(state)
			# 	all_this.extend(this)
			# 	all_inputs.extend(inp)
			# 	all_outputs.extend(out)

			rand = np.random.random(np.array(self.train_inputs).shape)
			tmp = np.array(self.train_inputs)*1.
			normed_tmp = tmp/ np.linalg.norm(tmp)
			self.train_inputs = (normed_tmp).tolist()
			self.train_inputs = (tmp).tolist()
			# import pdb; pdb.set_trace()
			# self.train_inputs += .01*rand
			state, _output, this = self.stepper(
					np.array(self.train_inputs),
					np.array(self.train_outs),
					1.)
			all_states.extend(state)
			all_this.extend(this)
			all_inputs.extend(self.train_inputs)
			all_outputs.extend(self.train_outs)
			weights_out = self.trainer(all_states,all_this)
			'''

			for pad,io in zip(trainp,trainv):
				# io = np.array(io)
				# pad = np.array(pad)

				inpu= io[0]
				outpu = io[1]

				padi = pad[0]
				pado = pad[1]


				# state, _output, this = self.stepper(
				# 		padi,
				# 		pado,
				# 		0.)

				inpu = (np.array(inpu)*.8).tolist()
				state, _output, this = self.stepper(
						inpu,
						outpu,
						1.)

				import pdb; pdb.set_trace()
				all_states.extend(state)
				all_this.extend(this)
				all_inputs.extend(inpu)
				all_outputs.extend(_output)
				weights_out = self.trainer(all_states,all_this)
			'''
			# M_tonos = np.linalg.pinv(all_states)
			# all_this = np.arctanh(all_this)
			# W_trans = np.dot(M_tonos,all_this)
			# self.esn.W_out.set_value(W_trans)
			# import pdb; pdb.set_trace()
			import matplotlib.pyplot as plt
			f, axarr = plt.subplots(3, sharex=True)
			axarr[0].plot(all_outputs,label="output")
			axarr[0].set_title('Network Output')
			# axarr[0].set_xlabel('Time')
			axarr[0].set_ylabel('Values')
			# axarr[0].legend()
			axarr[1].plot(all_states,label="state")
			axarr[1].set_title('Network State')
			# axarr[1].set_xlabel('Time')
			axarr[1].set_ylabel('Values')

			# axarr[1].legend()
			axarr[2].plot(all_inputs,label="inputs")
			axarr[2].set_title('Network Inputs')
			axarr[2].set_xlabel('Time')
			axarr[2].set_ylabel('Values')
			# plt.show()

			##TEST
			all_states = []
			all_this = []
			all_inputs = []
			all_outputs = []

			# for inp,out in zip(self.train_inputs,self.train_outs):
				#create a random pad
				# padIn = createPad(100,3,self.indim)
				# padOut = createPad(100,3,self.outdim)
				# #pass it through the net

				# _,_,_ = self.stepper(
				# 		padIn,
				# 		padOut,
				# 		.0)
				#pass the values to train
				# state, _output, this = self.stepper(
				# 		inp,
				# 		out,
				# 		.0)
				# # import pdb;pdb.set_trace()
				# all_states.extend(state[:,:self.units])
				# all_this.extend(this)
				# all_inputs.extend(inp)
				# all_outputs.extend(_output)
			state, _output, this = self.stepper(
					self.train_inputs,
					self.train_outs,
					.0)
			# import pdb;pdb.set_trace()
			all_states.extend(state[:,:self.units])
			all_this.extend(this)
			all_inputs.extend(self.train_inputs)
			all_outputs.extend(_output)

			# fig = plt.figure()
			# ax1 = fig.add_subplot(3, 1, 1)
			# ax2 = fig.add_subplot(3, 1, 2)
			# ax3 = fig.add_subplot(3, 1, 3)
			# ax1.plot(all_outputs,label="output")
			# ax1.set_title('Network Output')
			# # ax1.set_xlabel('Time')
			# ax1.set_ylabel('Values')
			# # axarr[0].legend()
			# ax2.plot(all_states,label="state")
			# ax2.set_title('Network State')
			# # ax2.set_xlabel('Time')
			# ax2.set_ylabel('Values')
			#
			# # axarr[1].legend()
			# ax3.plot(all_inputs,label="inputs")
			# ax3.set_title('Network Inputs')
			# ax3.set_xlabel('Time')
			# ax3.set_ylabel('Values')

			f, axarr = plt.subplots(3, sharex=True)
			axarr[0].plot(all_outputs,label="output")
			axarr[0].set_title('Network Output')
			# axarr[0].set_xlabel('Time')
			axarr[0].set_ylabel('Values')
			# axarr[0].legend()
			axarr[1].plot(all_states,label="state")
			axarr[1].set_title('Network State')
			# axarr[1].set_xlabel('Time')
			axarr[1].set_ylabel('Values')

			# axarr[1].legend()
			axarr[2].plot(all_inputs,label="inputs")
			axarr[2].set_title('Network Inputs')
			axarr[2].set_xlabel('Time')
			axarr[2].set_ylabel('Values')

			if self.folderName == None:
				plt.show()
			else:
				filename = self.folderName+"/user_ann_training_data.pickle"
				self.esn.save(filename)

				filename = self.folderName+"/user_ann_training_dataIN.pickle"
				f = open(filename,"wb")
				pickle.dump(all_inputs,f)
				f.close()

				filename = self.folderName+"/user_ann_training_dataOUT.pickle"
				f = open(filename,"wb")
				pickle.dump(all_outputs,f)
				f.close()

				filename = self.folderName+"/user_ann_training_dataSTATE.pickle"
				f = open(filename,"wb")
				pickle.dump(all_states,f)
				f.close()
				# file = open(folder+"/user_ann_training_data.pickle","wb")
				# dtsets=[]
				# dtsets.append(weights_out)
				# dtsets.append(self.esn.W)
				# pickle.dump(dtsets,file)

				plt.show()
				plt.savefig(self.folderName+"/user_ann_training_plot.pdf",figsize=(15, 8), dpi=290)
			''''''
			# print W_trans

	def plot(self,):
		# fig = plt.figure( sharex=True)
		f, axarr = plt.subplots(3, sharex=True)
		# ax1 = fig.add_subplot(3, 1, 1)
		# ax2 = fig.add_subplot(3, 1, 2, sharex=ax1)
		# ax3 = fig.add_subplot(3, 1, 3, sharex=ax1)

		plot_output = np.array(self.all_outputs)
		for idx in range(self.outdim):
		 	column = plot_output[:,idx]
			# ax1.plot(self.all_outputs,label="output")
			axarr[0].plot(column,label="O"+str(idx))
			axarr[0].set_title('Network Output')
			# axarr[0].set_xlabel('Time')
			axarr[0].set_ylabel('Values')
		axarr[0].legend()

		axarr[1].plot(self.all_states,label="state")
		axarr[1].set_title('Network State')
		# axarr[1].set_xlabel('Time')
		axarr[1].set_ylabel('Values')

		# axarr[1].legend()
		axarr[2].plot(self.all_inputs,label="inputs")
		axarr[2].set_title('Network Inputs')
		axarr[2].set_xlabel('Time')
		axarr[2].set_ylabel('Values')

		if self.folderName == None:
			plt.show()
		else:
			filename = self.folderName+"/user_ann_use_data.pickle"

			file = open(self.folderName+"/user_ann_use_in_data.pickle","wb")
			pickle.dump(self.all_inputs,file)

			file = open(self.folderName+"/user_ann_use_state_data.pickle","wb")
			pickle.dump(self.all_states,file)

			file = open(self.folderName+"/user_ann_use_out_data.pickle","wb")
			pickle.dump(self.all_outputs,file)

			plt.show()
			# plt.savefig(self.folderName+"/user_ann_use_plot.pdf",figsize=(15, 8), dpi=290)

	def serv_step(self, val_in, wash_out = False):
		# import pdb; pdb.set_trace()
		if type(val_in) == dict:
			val_in = np.array(val_in.values())

		self.rollingInputs.append(val_in)
		self.rollingInputs = self.rollingInputs[1:]
		# self.idx +=1
		# self.esn.state_z = self.state
		mse = np.mean((np.array(self.resting_pos) - np.array(val_in))**2)
		# print "MSE ",mse
		if mse > -0.1:
			# self.state, output, this = self.stepper(
			# 	np.array(self.rollingInputs), self.outputs, 0.)
			self.state, output, this = self.step(
				val_in)
			# output -= np.std(output)
			# output = output- np.amin(output)
			# if output.any() == 0.0:
			# 	return 0
			# output = output/ np.amax(output)
			self.output_sliding_window.extend([output.tolist()])
			self.output_sliding_window = self.output_sliding_window[1:]

		# import pdb; pdb.set_trace()

		outpuT = np.mean(self.output_sliding_window,axis=0) *10.
		'''
		outpuT = output
		'''
		# np.asarray(
		# 	self.output_sliding_window).sum(axis=0) / self.output_window
		# outpuT /= np.linalg.norm(outpuT)
		# logging.info("NET OUT real "+ str(outpuT))
		tmp = np.zeros_like(outpuT)
		tmp[np.argmax(outpuT)] = 1.

		print "Activated Expert: #",np.argmax(outpuT)
		# outpuT = tmp

		# if len(self.all_states) > 300:
		# 	self.all_states = self.all_states[1:]
		# 	self.all_inputs = self.all_inputs[1:]
		# 	self.all_outputs = self.all_outputs[1:]
		'''
		self.all_states.extend(self.state[:,:self.units])
		self.all_inputs.append(self.rollingInputs[-1])
		self.all_outputs.append(outpuT)
		'''
		self.all_states.append(self.state[:self.units])
		self.all_inputs.append(val_in)
		self.all_outputs.append(outpuT)



		# logging.info("NET OUT "+ str(outpuT))
		data = {
			"action":"activate",
            "source":"input_control",
            "destination":"robot_control",
            "value":outpuT.tolist()#tmp.tolist()
		}
		if not wash_out:
			emit(
				'robot_control',
				data,
				namespace='/control_background',
				broadcast=False
				)

		return True#self.state, output
		'''
		self.line1.set_xdata(range(len(self.all_outputs)))
		self.line2.set_xdata(range(len(self.all_states)))
		self.line3.set_xdata(range(len(self.all_inputs)))

		# import pdb;pdb.set_trace()
		linesfor1 = np.array(self.all_outputs).reshape(
			len(self.all_outputs[0]),len(self.all_outputs))
		for line in linesfor1:
			self.line1.set_ydata(line)

		linesfor2 = np.array(self.all_states).reshape(
			len(self.all_states[0]),len(self.all_states))
		for line in linesfor2:
			self.line2.set_ydata(line)

		linesfor3 = np.array(self.all_inputs).reshape(
			len(self.all_inputs[0]),len(self.all_inputs))
		for line in linesfor3:
			self.line3.set_ydata(line)

		self.fig.canvas.draw()
		outpuT = np.random.random(self.indim)
		'''


	def back_end_control(self,data):

		if data is not None:
			if data['source'] == "front":
				if data['action'] == "switch":
					if data['value']:
						self.thread = None
					else:
						self.serv_close()
				elif data['action'] == "training_values":
					if data['value']:
						self.serv_train(data['value'])
					else:
						#training has stopped
						self.train()
						logging.warning(
							"input_control::back_end_control, \
							train data not valid"
						)
				elif data['action'] == "step":
					# print data['value']
					if data['value']:
						if self.once:
							self.init_live_graph()
						self.serv_step(data['value'])
					else:
						self.plot()

						#reset the simulator
						data = {
							"action":"reset",
				            "source":"input_control",
				            "destination":"robot_control",
				            "value":True
						}
						emit(
							'robot_control',
							data,
							namespace='/control_background',
							broadcast=False
							)

						logging.warning(
							"input_control::back_end_control, \
							train data not valid"
						)
				elif data['action'] == "wash_out":
					if data['value']:
						#make a wash_out
						# wash_out = 0.*createPad(1,val_size=self.indim)[0]
						wash_out = []
						for i in xrange(self.indim):
							wash_out.append(0.01*next(self.sine))
						self.serv_step(wash_out,wash_out=True)
					else:
						logging.warning(
							"input_control::back_end_control, \
							wash out not valid"
						)
				elif data['action'] == "reset":
					if data['value']:
						#list of lists for training
						self.train_outs = []
						self.train_inputs = []
						emit(
							'notification',
							"ESN was reset!",
							namespace='/control',
							broadcast=False
							)
					else:
						logging.warning(
							"input_control::back_end_control, \
							reset"
						)
				else:
					logging.warning(
						"input_control::back_end_control::front, \
						 data[source] not recognised"
					)
			elif data['source'] == "interface":
				pass
			elif data['source'] == "robot_control":
				#Here can be feedback from the robot
				#controlling module
				pass
			else:
				logging.warning(
				"input_control::back_end_control, \
				data[source] not recognised"
				)
		else:
			logging.warning(
				"input_control::back_end_control, data is None"
				)
