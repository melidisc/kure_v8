from  tools_module import *

class Controller:
	def __init__(self, func):
		self.cont_class = func
		self.trainer = self.cont_class.train
		self.stepper = self.cont_class.step

	@logger
	def train(self,):
		return self.trainer()
	@logger
	def step(self,):
		return self.stepper()

if __name__=="__main__":

	test = Controller()
	test.train()
	test.step()
