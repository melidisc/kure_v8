import gsocketpool.pool
from mprpc import RPCPoolClient

client_pool = gsocketpool.pool.Pool(RPCPoolClient, dict(host='127.0.0.1', port=6001))
client_pool2 = gsocketpool.pool.Pool(RPCPoolClient, dict(host='127.0.0.1', port=6000))

with client_pool.connection() as client:
    print client.call('ack', 1)
# with client_pool2.connection() as client:
#     print client.call('sum', 1, 2)
