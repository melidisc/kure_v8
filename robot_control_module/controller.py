from  tools_module import *
from pybrain.rl.environments.ode.sensors import *

from so_sml_v2.hm.homeokinesis_v2 import Homeokinesis
from so_sml_v2.experts.expert import Experts
#instance_ccrl_bare
#instance_sphere_walker
#instance_arm
#instance_octacrawl
#instance_ccrl_bare
from so_sml_v2.instances import *#instance_ccrl_bare
from threading import Thread
import numpy as np
from time import sleep
from flask.ext.socketio import SocketIO, emit

import matplotlib.pyplot as plt

import logging
import os
logging.basicConfig(level=logging.INFO)

global sim_act
sim_act = None
global sim_sens
sim_sens = None

class Controller:
	def __init__(self, f):
		self.initialised = False
		self.path = f

	def initialise(self,dt):
		self.initialised = True
		#world learning rate
		self.wlr = 0.01
		#controller learning rate
		self.clr = .1
		self.hmkn = Homeokinesis(self.robot_sim, self.wlr, self.clr)
		self.hmkn.dt = dt
		# self.robot_sim.addSensor(
		# 			SpecificBodyPositionSensor(['target'], "target"))

		self.exp_num = 7
		self.ex_lr = 0.1


		if os.path.isfile(self.saving_path+".pickle") :
			import pickle
			self.exps = pickle.load(open(self.saving_path+".pickle","rb"))
			msg = "Loaded experts: ", [x["dtset"] for x in self.exps.experts]
			logging.info(msg)
		else:
			self.exps = Experts(
				self.exp_num,
				2*self.hmkn.sensors,
				self.hmkn.sensors+self.hmkn.actuators,
				50*self.hmkn.sensors
				)
		print "Experts inited with network| in:",2*self.hmkn.sensors,\
	 		" hidden:",50*self.hmkn.sensors," out:",self.hmkn.sensors+\
			self.hmkn.actuators


		self.thread = None
		self.thread_unlocked = True
		self.x_tm1 = np.zeros(self.hmkn.sensors)
		self.sim_on = True
		self.sim_thread = None
		# self.folderName = None
		emit(
			'notification',
			"Homeokinetic controller initialised",
			namespace='/control',
			broadcast=False
			)
	def setup_robot(self, data):
		if self.initialised:
			self.robot_sim.closeSocket()
			del self.robot_sim
			self.close()

		if data['switch']:
			robot = data['device']
			self.robot_name = robot
			self.saving_path = self.path+"/saves/behaviours/"+ self.robot_name
			msg = "Saving path: ",self.saving_path
			logging.info(msg)
			dt = 2
			if robot == "ccrl":
				self.robot_sim = instance_vrep.VrepObj()
				# self.robot_sim =  instance_ccrl_bare.CCRLEnv()
				# self.robot_sim.addSensor(
				# 	SpecificBodyPositionSensor(['armUpLeft'], "bodyPos"))
				dt = 13
			elif robot == "ccrl_stif":
				self.robot_sim =  instance_ccrl_stif_bare.CCRLEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['armUpLeft'], "bodyPos"))
				dt = 13
			elif robot == "sphere_walker":
				self.robot_sim = instance_sphere_walker.SphereWalkerEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['leg1'], "bodyPos"))
				dt = 13
			elif robot == "acrobot":
				self.robot_sim = instance_acrobot.AcrobotEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['leg1'], "bodyPos"))
				dt = 3
			elif robot == "arm":
				self.robot_sim = instance_arm.ArmEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['hip'], "bodyPos"))
				dt = 5
			elif robot == "ball":
				self.robot_sim = instance_ball.BallEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['body'], "bodyPos"))
				dt = 2
			elif robot == "octacrawl":
				self.robot_sim = instance_octacrawl.OctacrawlEnv()
				self.robot_sim.addSensor(
					SpecificBodyPositionSensor(['leg1'], "bodyPos"))
			self.initialise(dt)

	def close(self,):
		if self.sim_thread:
			self.sim_on = False
			self.sim_thread.join(1)
		if self.thread:
			self.thread_unlocked = False
			self.thread.join(1)
		return True
	def train(self,):
		while self.thread_unlocked:
			#make a step
			x_tm1, x_t, x_tp1, m_t = self.hmkn.step()
			print "Homeokinesis motor: ",m_t
			inp = np.append(x_t, x_tm1)
			outp = np.append(x_tp1, m_t)

			#train the experts experts prediction
			# print "Experts inp: ",inp," outp: ", outp
			self.exps.train(inp, outp, self.ex_lr)

			#learn from it
			self.hmkn.learn()

			sleep(0.00001)

	# def step_run(self,):
	# 	while self.thread_unlocked:


	def step(self, sml = True, nExp = None,  expVals=None):
		global sim_act
		global sim_sens

		if sml:
			x_tm1, x_t, x_tp1, m_t = self.hmkn.step()
			return x_tm1, x_t, x_tp1, m_t
		else:
			#activate expert

			#get new sim readings
			# x_t = self.getSens()
			x_t = sim_sens

			inp = np.append(x_t, self.x_tm1)

			#store old readings
			self.x_tm1 = x_t

			if nExp != None:


				output =  self.exps.step(inp, nExp)[0]
				sensors = output[:self.hmkn.sensors]
				actuators = output[self.hmkn.sensors:]

				#act with the expert
				sim_act = actuators
				# self.applyMotor(actuators)

				return True

			else:
				# print "inp",inp

				output =  self.exps.step(inp)

				actuators = np.zeros(self.hmkn.actuators)

				for o, ev in zip(output,expVals):
					o = np.clip(o[0][-self.hmkn.actuators:],-1.,1.)
					# print "O",o,"EV",ev,"ACT",o*ev
					actuators += o*ev
					#we have a list of the values

				#act with the expert
				# if actuators.all() == .0 :
				# 	return False

				# print "sim_act",actuators
				# self.applyMotor(actuators)
				sim_act = actuators
				return True
	def getSens(self,):
 		return np.array(
 			self.robot_sim.getSensorByName("JointSensor")
 			)
	def applyMotor(self,act):
		# import pdb;pdb.set_trace()
		# act = np.array(act)/np.linalg.norm(act)
		# act +=1.
		# act /=2.
		# print "daamn ",act
		return self.robot_sim.performActionNew(
			act
		)

	def sim_step(self):
		global sim_act
		global sim_sens
		while self.sim_on:
			sim_sens = self.hmkn.getSensors()

			if sim_act is not None:
				# print "Simulation action ", sim_act
				self.applyMotor(sim_act)
			self.robot_sim.step()
			sleep(0.00001)

	def sim_switch_on(self):
		if (self.sim_thread is None):
			self.sim_on = True
			self.sim_thread = Thread(target=self.sim_step)
			self.sim_thread.start()
			logging.info( "Thread Started")

		else:
			logging.info( "THREAD SIM DUNNON")
	def sim_switch_off(self):
		if (self.sim_thread is not None):
			self.sim_on = False
			self.sim_thread.join(1)
			self.sim_thread = None
			logging.info("Thread Stoped")
		else:
			logging.info("THREAD SIM DUNNOFF")

	def save(self, filename):
		self.exps.save(filename)

	def load(self, filename):
		self.exps.load(filename)


	def back_end_control(self,data ):
		global sim_act
		if data is not None:
			if data['source'] == "front":

				if data['action'] == "switch":
					if data['value']:
						self.thread = None
					else:
						self.close()

				elif data['action'] == "train":
					if data['value']:
						try:
							self.sim_switch_off()
							self.thread_unlocked = True
							self.thread = Thread(target=self.train)
							self.thread.start()
						except Exception as e:
							logging.warning(
								"robot_control::back_end_control, \
								train data not valid ", e
							)
							return False
						else:
							return True
					else:
						#training is stoped
						self.thread_unlocked = False
						self.thread.join(1)

						# self.plot_training_data()
						if self.robot_name != None and not os.path.isfile(self.saving_path+".pickle"):
							msg = "Saving behaviours for ",self.robot_name
							logging.info(msg)
							self.exps.save(self.saving_path, absolute=True)
						if self.folderName != None:
							self.save_training_data(self.folderName)
						expertsList = []
						for ex_id, ex in enumerate(self.exps.experts):
							if len(ex["dtset"])>1:
								expertsList.append(ex_id)

						data = {
							"action":"experts_trained",
		                    "source":"robot_control",
		                    "destination":"front",
		                    "value":expertsList#self.exp_num
						}
						emit(
							'robot_control',
							data,
							namespace='/control_background',
							broadcast=False
							)
						# self.sim_switch_on()



				elif data['action'] == "step":
					logging.info(
							"robot_control::back_end_control,"+\
							str(data['value'])
						)
					if data['value']:
						self.sim_switch_on()
						self.step()
						# self.robot_sim.dt /= 10
						return True
					else:
						# self.sim_switch_off()

						logging.info(
							"robot_control::back_end_control, \
							stopping the steps"
						)
						return False

				elif data['action'] == "activate":
					if data['value']:
						if data['value'] == -1:
							sim_act = None
						else:
							vals = np.zeros(self.exp_num)
							for v in data['value']:
								vals[v] = 1./len(data['value'])
							# print "vals for experts ", vals
							self.step(sml=False,expVals=vals)
					else:
						# self.sim_switch_off()
						logging.warning(
						"robot_control::back_end_control::front, \
						 data[source] not recognised"+str(data)
					)
				else:
					logging.warning(
						"robot_control::back_end_control::front, \
						 data[source] not recognised"+str(data)
					)
			elif data['source'] == "interface":
				pass

			elif data['source'] == "input_control":
				#Here control the robot with the experts
				# import pdb; pdb.set_trace()

				if data['action'] == 'activate':
					if data['value']:
						# print data
						# self.sim_switch_on()
						# print "vals for experts ", data["value"]
						self.step(sml=False, expVals=data['value'])
						# self.step(sml=False, nExp=np.argmax(data['value']))
						logging.info(
							"robot_control::back_end_control::input_control"+
							"\n activating expert "
						)
					else:
						# self.sim_switch_off()
						# self.thread_unlocked = False
						# self.thread.join(1)
						pass
					return True
				elif data['action'] == 'reset':
					self.robot_sim.reset()
				else:

					logging.warning(
						"robot_control::back_end_control::input_control, \
						 data[action] not recognised"
					)
					return False

			else:
				logging.warning(
				"robot_control::back_end_control, \
				data[source] not recognised"
				)
		else:
			logging.warning(
				"robot_control::back_end_control, data is None"
				)
	def plot_training_data(self,save_plots=False):

		f, axarr = plt.subplots(self.exp_num, 2, sharex=False,sharey=False)
		if not save_plots:
			f2, axarr2 = plt.subplots(self.exp_num, sharex=False,sharey=False)
		for exp_id, exp in enumerate(self.exps.experts):
			exp_data_plot = []

			for v in exp['dtset']:
				exp_data_plot.append(
					exp["step"](v[0])[0][self.hmkn.sensors:]
					)

			if exp_data_plot!= []:
				axarr[exp_id,0].plot(exp_data_plot,label="output")
				axarr[exp_id,0].set_title('(A) Expert No. '+str(exp_id+1))
				axarr[exp_id,0].set_xlabel('Time')
				axarr[exp_id,0].set_ylabel('Output Values')

				pad = 30
				if len(exp['dtset']) > pad:
					gt = np.array(exp['dtset'][pad:])[:,1,:self.hmkn.sensors]
					gt_ph = np.array(exp['dtset'][:-pad])[:,1,:self.hmkn.sensors]
					# axarr[exp_id,1].set_axis_off()
					axarr[exp_id,1].set_xlabel('Sensor values t')
					axarr[exp_id,1].set_ylabel('Velocity')
					axarr[exp_id,1].set_title('(B) Expert No.'+str(exp_id+1))
					c = [float(x)/len(gt[0]) for x in range(len(gt[0]))]
					c = np.tile(c,len(gt)).reshape(len(gt),len(gt[0]))
					axarr[exp_id,1].scatter(gt,(gt_ph-gt)/pad, s=15, c=c, alpha=0.5)


					gt = np.array(exp['dtset'][pad:])[:,1,self.hmkn.sensors:]
					gt_ph = np.array(exp['dtset'][:-pad])[:,1,self.hmkn.sensors:]

					c = float(exp_id)/len(self.exps.experts)

					c = np.tile(c,len(gt)).reshape(len(gt),1)
					# import pdb;pdb.set_trace()
					if not save_plots:
						axarr2[exp_id].set_axis_off()
						# axarr2[exp_id].scatter(gt, gt_ph, s=15, c=c , alpha=0.5)
						axarr2[exp_id].plot(gt, (gt_ph-gt)/pad)
						axarr2[exp_id].set_title('Model '+str(exp_id+1))
		if save_plots:
			return plt
		else:
			plt.show()
	def save_training_data(self,folder):
		import pickle
		file = open(folder+"/controller_training_data.pickle","wb")
		dtsets=[]
		for ex in self.exps.experts:
			dtsets.append(ex["dtset"])
		pickle.dump(dtsets,file)

		plt = self.plot_training_data(save_plots=True)
		plt.savefig(folder+"/controller_training_plot.pdf",figsize=(15, 8), dpi=190)


# if __name__=="__main__":

# 	test = Controller()
# 	test.train()
# 	test.step()
