#!/usr/bin/env python

import numpy as np
import sys
import signal
import pylab as plt
import functools
import pdb
import pickle
import threading


# from pybrain.rl.environments.ode import ODEEnvironment, actuators
# from pybrain.rl.environments.ode.sensors import *
from instance_octacrawl import OctacrawlEnv
from instance_arm import ArmEnv
from instance_acrobot import AcrobotEnv
from instance_box_shpere import BoxSphereEnv
from pybrain.auxiliary import GradientDescent
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.tools.neuralnets import buildNetwork
from pybrain.structure import TanhLayer,SigmoidLayer
from pybrain.datasets import SupervisedDataSet

import theano 
import theano.tensor as T 

##server##
from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
from cgi import parse_header, parse_multipart

#interface check
#TODO replace this ugliness
global learning 	
global controling_expert_parameters 	
global controling_experts_on 	
learning = True
controling_expert_parameters = [-1,-1,-1]
controling_experts_on = []

def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)

    try:
        if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
            mlctrl.save_weights()
            server.socket.close()
            sys.exit(1)

    except KeyboardInterrupt:
        print("Ok ok, quitting")
        sys.exit(1)

    # restore the exit gracefully handler here    
    signal.signal(signal.SIGINT, exit_gracefully)


def jacobi(A,b,N=25,x=None):
    """Solves the equation Ax=b via the Jacobi iterative method."""
    # Create an initial guess if needed                                                                                                                                                            
    if x is None:
        x = np.zeros(A.shape[0])

    # Create a vector of the diagonal elements of A                                                                                                                                                
    # and subtract them from A                                                                                                                                                                     
    D = np.diag(A)
    #Taking into account the bias values
    # W = [np.append(x,.0) for x in np.diagflat(D)]
    W=np.diagflat(D)
    R = A[:,:-1] - W
    # pdb.set_trace()
    # Iterate for N times                                                                                                                                                                          
    for i in xrange(N):
        x = (b - np.dot(R,x) + A[:,-1]) / D
    return x

def subtract_norm(sensor_pred, sensor_real=None):
	if sensor_real == None:
		sensor_real = re.reform(env.getSensorByName("JointSensor"))
	return np.sqrt(np.sum(np.power(np.subtract(sensor_real, sensor_pred),2)))

def auto_diff(f):
    """
    Automatically create a function that computes directional derivatives of f.
    
    Returns: A function df(x, v) which computes D_v f(x).
    """
    functools.wraps(f)
    def derivative(x,  v, mex=None, *args, **kwargs):
        # v = v.reshape((1,-1))
        # pdb.set_trace()
        return f(mex[1].reshape(15,1) + np.dot(v[:,:-1],mex[1].reshape(15,1))*  1j * sys.float_info.min, sensor_real=mex[0]).imag / sys.float_info.min
    return derivative

def optimize_with_autodiff(f, x_start, eta, args=None):
    """
    Optimize the function f, starting from x_start using automatic
    differentiation.  x_start should be a size (1, d) matrix.
    
    This function runs T iterations of an SPSA-like algorithm with step size eta.
    
    Returns: A size (T, d) matrix with the trajectory of points during optimizaiton.
    
    """

    df = auto_diff(f)
    # Choose a random direction
    v = np.random.normal(size=x_start.shape)
    v[:,:-1] /= np.sqrt(np.dot(v[:,:-1],v[:,:-1]))
    # Take a step along v or -v (whichever reduces the objective locally)
    
    if (args!=None):
        return x_start - eta * df(x_start, v, mex=args ) * v
    else:
        return x_start - eta * df(x_start, v) * v
    # return x
def calculateE(sensort_t_1, motor_t_1, sensor_t, desens= 0.0, square=False):
	'''
		Calculate z based on the delayed inputs since the present input x is
		produced by the outputs tau time steps before
		which on their hand are y = K(x_D)
		due to the delay in the feed back loop.
	'''
	#get controller output for t-1
	z = ctrl.step(stateFull=False, old=2)#ctrl.weights_c[:,:-1] * sensort_t_1 + ctrl.weights_c[:,-1].reshape(ctrl.n,1)

	#get xsi for t - world model error
	xsi = sensor_t - wrld.weights_a[:,:-1] * motor_t_1 + wrld.weights_a[:,-1].reshape(wrld.n,1)
	# //Matrix xsi = x_buffer[t%buffersize] - A * z.map(g);

	#
	Cg = g_s(ctrl.weights_c[:,:-1] * z + ctrl.weights_c[:,-1]) #// Cg_{ij} = g'_i * C_{ij}
	L = wrld.weights_a[:,:-1]*Cg + wrld.weights_a[:,-1]                   #// L_{ij}  = \sum_k A_{ik} g'_k c_{kj}
	# pdb.set_trace()
	v = np.array((np.linalg.det(L)*L)*xsi)
	# pdb.set_trace()
	if square:
		E = ((v.T)*v)
	else:
		E = ((v.T)*v)[0,0]
	Es = 0.0
	if(desens!=0):
		diff_x = sensor_t - wrld.weights_a[:,:-1]*( np.tanh(ctrl.weights_c[:,:-1]*sensor_t+ctrl.weights_c[:,-1].reshape(ctrl.n,1)) )+wrld.weights_a[:,-1].reshape(wrld.n,1);
		Es = ((np.asarray(diff_x).T)*diff_x)[0, 0];
	
	return (1-desens)*E + desens*Es

def g_s(matrix):
	'''
		Soft first derivative of g()
	'''
	#k=np.tanh(matrix)
	#return 1.0 - k*k
	return sigmoid(matrix)*(1-sigmoid(matrix))

class Controller:
	def __init__(self,sensor_vector_size, motor_vector_size, smooth_function, buffer_size, weights=None):
		self.n = sensor_vector_size
		self.fun = smooth_function
		self.buff_s = buffer_size
		# self.c_bias = np.zeros((motor_vector_size,1))
		#weight == sensor lenght + bias
		if weights == None:
			self.weights_c = np.random.rand(sensor_vector_size, sensor_vector_size+1)
		else:
			self.weights_c = weights
		self.sensor_buffer = np.zeros((self.buff_s,self.n))
		self.t = 0
		self.motor_values_produced = np.array([])
		self.E_0_old=.0,.0

	def step(self, sensor_reading=None, weights=None, stateFull=True, old=None):
		
		if weights!=None:
			self.weights_c = weights
		
		if (old !=None) & (old > 0):
			index = (self.t-2-old)%self.buff_s
		else:
			index = (self.t-1)%self.buff_s
		
		if(sensor_reading==None) :
			sensor_reading = self.sensor_buffer[index]
		
		#cyclic table for the sensor readings
		if (stateFull) :
			self.sensor_buffer[self.t%self.buff_s] = sensor_reading
			self.t +=1
		

		#controller functionality
		if (self.t >= self.buff_s):
			ret = np.multiply(self.weights_c[:,:-1], self.sensor_buffer[index].reshape((1,self.n)))
		else:
			ret = np.multiply(self.weights_c[:,:-1], sensor_reading.reshape((1,self.n)))
		ret += self.weights_c[:,-1].reshape(self.n,1)

		# self.motor_values_produced = np.append(self.motor_values_produced, self.fun(ret)[0]).reshape(self.t, self.n)
		return self.fun(ret)[0]
	
	def stepNoLearn(self, sensor_reading):
		self.sensor_buffer[self.t%self.buff_s] = sensor_reading
		self.t +=1

	def learn(self, sense_reverse, motor_reverse, sensor_real, learning_rate):
		if not (self.t >= self.buff_s):
			return np.array([0])
		
		# args=[self.sensor_buffer[(self.t-1)%self.buff_s], sense_reverse]
		# self.weights_c = optimize_with_autodiff(func, self.weights_c, eta=learning_rate, args=args)
		damping_rate = 0
		delta = 0.001

		# print self.weights_c.shape

		E_0_m = calculateE(sense_reverse, motor_reverse, sensor_real, square=True)
		E_0_v = calculateE(sense_reverse, motor_reverse, sensor_real, square=False)
		# print "\t TMP ",(calculateE(sense_reverse, motor_reverse, sensor_real) - self.E_0_old)
		sense_reverse = self.sensor_buffer[(self.t-1)%self.buff_s]
		motor_reverse = wrld.motor_buffer[(wrld.t-1)%wrld.buff_s]

		#update the bias
		self.weights_c[:,-1] += delta
		tmp = - learning_rate * (calculateE(sense_reverse, motor_reverse, sensor_real,square=False) - E_0_v )/ delta #self.E_0_old[1]
		# print "tmp ", tmp
		self.weights_c[:,-1] -= delta
		#the real update
		self.weights_c[:,-1] += np.clip(tmp, -0.1, 0.1)

		#update the weights
		self.weights_c[:,:-1] += delta
		tmp = - learning_rate * (calculateE(sense_reverse, motor_reverse, sensor_real,square=True) - E_0_m) / delta #self.E_0_old[0]
		tmp -= damping_rate * self.weights_c[:,:-1]
		# print "tmp2 ", tmp
		self.weights_c[:,:-1] -= delta
		#the real update
		self.weights_c[:,:-1] += np.clip(tmp, -0.1, 0.1)
		self.E_0_old = E_0_m, E_0_v
		

		return self.weights_c

	def reverse_step(self, motor_reading, weights=None, stateFull=None):
		# return motor_reading/self.weights_c[0][:-1] - self.weights_c[0][-1]
		return jacobi(self.weights_c, motor_reading, N=25)

class World_Model:
	def __init__(self,motor_vector_size, sensor_vector_size, buffer_size, weights=None):
		self.n = motor_vector_size
		self.buff_s = buffer_size
		# self.m_bias = np.zeros((sensor_vector_size,1))
		if weights == None:
			self.weights_a = np.random.rand(motor_vector_size, motor_vector_size+1)
		else:
			self.weights_a = weights	
		self.motor_buffer = np.zeros((self.buff_s,self.n))
		self.t = 0
	def step(self, motor_reading, weights=None, stateFull=True):
		if weights!=None:
			self.weights_a = weights
		
		#cyclic buffer 
		if stateFull:
			self.motor_buffer[self.t % self.buff_s] = motor_reading
			self.t +=1
		
		#implementation of the World World_Model
		if (self.t >= self.buff_s):
			ret = np.multiply(self.weights_a[:,:-1], self.motor_buffer[(self.t-2)%self.buff_s].reshape((self.n,1)))
		else:
			ret = np.multiply(self.weights_a[:,:-1], motor_reading.reshape((self.n, 1)))
		
		ret += self.weights_a[:,-1].reshape(self.n,1)

		return ret
	def reverse_step(self, sensor_reading, weights=None, stateFull=None):
		# return sensor_reading/self.weights_a[0][:-1] - self.weights_a[0][-1]
		return jacobi(weights_wrld, sensor_reading, N=25)

class Reform:
	def __init__(self, places):
		self.un = places
	def reform(self,ar):
		return np.array(ar)
		# return np.delete(ar, self.un)

class MultiExperts():
	
	def __init__(self, number, buffer_size, smooth_value=0.5, inp_size=30, out_size=30):
		self.n = number
		self.b = smooth_value
		self.inp_size = inp_size
		self.hiddenl = 13
		self.out_size = out_size
		self.buffer = np.zeros(buffer_size)
		self.nets = []
		self.trainers = []
		self.ds = []
		self.smoothed_errors = np.zeros(self.n)
		self.errors_past = np.zeros(self.n)
		self.available_experts = []
		wff = False
		if self.load_weights() == 0 :
			wff = True
		for i in xrange(0,self.n):
			if wff:
				self.nets.append(buildNetwork(inp_size, self.hiddenl, out_size, bias=False, hiddenclass=SigmoidLayer))
			self.ds.append(SupervisedDataSet(inp_size, out_size))
			self.trainers.append(BackpropTrainer(self.nets[i], self.ds[i]))
		#if no weight file found make one
		self.fileObject = open('MultiExperts_'+str(inp_size)+'_'+str(self.hiddenl)+'_'+str(out_size), 'w')
			

	def step(self,inputs, outputs):
		outputs = outputs.reshape(1,len(outputs))
		# inputs = inputs.reshape(len(inputs),1)
		re = np.array([])
		#get output from every net
		for i in xrange(0,self.n):
			re = np.append(re,self.nets[i].activate(inputs))
		re = re.reshape(self.n, self.out_size)
		
		#find errors
		errors = np.array([np.sqrt(np.sum(np.power(n,2))) for n in re-outputs])
		#first time just the error
		smoothed_errors = np.array(self.b*errors)#+(1-self.b)*self.errors_past)
		# pdb.set_trace()
		maxi = np.max(errors-smoothed_errors)
		if maxi <0:
			maxi = 0
		index = np.argmin(errors + self.n*np.power(maxi,2))
		
		try:
			b=self.available_experts.index(index)
		except ValueError:
			self.available_experts.append(index)

		#train only one
		# ds = SupervisedDataSet(30, 30)
		self.ds[index].addSample(tuple(inputs),tuple(outputs.reshape(self.out_size,)))
		# temp = self.ds[index].getSample(-1)
		# self.ds[index].clear()
		# self.ds[index].addSample(temp[0], temp[1])
		# trainer = BackpropTrainer(self.nets[index], ds)
		
		#store the error values
		self.errors_past = smoothed_errors
		#train and return the error
		return index,self.trainers[index].train()
	
	def save_weights(self):
		pickle.dump(self.nets, self.fileObject)
		self.fileObject.close()
	
	def load_weights(self):
		name = 'MultiExperts_'+str(self.inp_size)+'_'+str(self.hiddenl)+'_'+str(self.out_size)
		print "Searching for expert weights..."
		try:
			fileObject = open(name,'r')
		except:
			print "Expert weights not found, initializing..."
			return 0
		else:
			print "Expert weights found "+name+", loading..."
			self.nets = pickle.load(fileObject)

########################################################################################~SERVER		

def geter():
	obj = []
	global controling_experts_on
	val = 5
	for i in xrange(0,mlctrl.n):
		for a in controling_experts_on:
			if a['id'] == i:
				val = a['value']
		objs={"name":"c"+str(i), "value":val, "active":False, "samples":mlctrl.ds[i].getLength() }
		obj.append(objs)
	return json.dumps(obj)
	
def seter(params):
	status, name,active,value = params
	try:
		if status == "true":
			status = True
		else:
			status = False
		
		if active == "true":
			active = True
		else:
			active = False
		
		value = float(value)
		id = int(name[1:])
		
	except:
		return "Error: wrong values aquired"
	#deal the learning process
	
	global learning 
	print learning
	if learning != status:
		env.reset()
	learning = status
	print "learning changed to status ", learning
	#set the expert in control
	global controling_expert_parameters
	controling_expert_parameters = [id,active,value]
	global controling_experts_on
	
	if active:
		for a in controling_experts_on:
			if a['id']==id:
				controling_experts_on.remove(a)
		controling_experts_on.append({"id":id,"value":value})
	elif not active:
		controling_experts_on.remove({"id":id,"value":value})
	print "set ",id," expert ",active
	return "OK robot"

class MyHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		self.end_headers()
		if self.path == '/get':
			# Insert your code here
			res = geter()
			print(self.wfile)
			self.wfile.write(res)
			self.wfile.close()
			# return 0
	
		elif self.path[:4] == '/set':
			datastring = str(self.path).split("set/")[1]
			status = datastring.split("/")[0] 
			name  = datastring.split("/")[1]
			active =  datastring.split("/")[2]
			value = datastring.split("/")[3]
			# Insert your code here
			res = seter([status,name,active,value])
			print(status+" "+name+" "+active+" "+ value)
			self.wfile.write(json.dumps("{ status : 'OK', name: '"+name+"', active: '"+active+"', value: '"+ value+"'}"))
			self.wfile.close()
			
def	init_server(server):
	# try:
    # server = HTTPServer(('localhost', 8085), MyHandler)
    print('Started http server')
    server.serve_forever()
	# except KeyboardInterrupt:
	#     print('^C received, shutting down server')
	#     server.socket.close()

#############################################################################
def sigmoid(inp):
	return 1.0 / (1.0 + np.exp(-inp))

if __name__ == '__main__' :
	
	# store the original SIGINT handler
	original_sigint = signal.getsignal(signal.SIGINT)
	signal.signal(signal.SIGINT, exit_gracefully)
	
	#start the server in a new thread
	server = HTTPServer(('localhost', 8085), MyHandler)
	t = threading.Thread(name='server', target=init_server, args=(server,))
	t.start()

	#init robot
	# env = BoxSphereEnv()
	# env = AcrobotEnv()
	# pdb.set_trace()
	env = OctacrawlEnv()
	# env = ArmEnv()

	re = Reform([3,7,11,15,19])
	sense = re.reform(env.getSensorByName("JointSensor"))
	# pdb.set_trace()
	print "sensor number ",len(sense)
	print "motor number ",env.indim
	#inti weights
	weights_ctrl = np.random.rand(len(sense), len(sense) + 1)
	weights_wrld = np.random.rand(env.indim, env.indim + 1)
	
	#init model
	ctrl = Controller(len(sense), env.indim, sigmoid, 2, weights_ctrl)
	wrld = World_Model(env.indim, len(sense), 2, weights_wrld)

	#init parameters
	wrld_lr = 0.1 
	ctrl_lr = 0.001 

	#experts init
	mlctrl = MultiExperts(9,0,inp_size = 2*len(sense), out_size = len(sense)+env.indim)	
	

	#gradient parameters
	precision = 0.000001
	gradient_old = 0
	gradient_new = 1

	x = np.array([])
	z = np.array([])
	x_path = np.array([])
	
	chk = None
	reset_time = 0

	plt.ion()
	fig = plt.figure()
	ax = fig.add_subplot(211)
	bx = fig.add_subplot(212)
	
	line1, = ax.plot([ (np.sum(n))/len(n) for n in weights_wrld[:,:-1] ] + weights_wrld[:,-1], 'r-')
	line2, = bx.plot([ (np.sum(n))/len(n) for n in weights_ctrl[:,:-1] ] + weights_ctrl[:,-1], 'b-')

	#random plot
	give_it_a_push = 0
	
	#control interface variabel
	# sens = np.array([])
	
	#output colector
	sum_out = np.array([])
	experts_sum = []
	sum_error = np.array([])
	while True:
	# for i in xrange(0,10000):
		if learning:
			
			sense = re.reform(env.getSensorByName("JointSensor"))
			
			#controller weights gradient descent with respect to ||u_t||^2
			a = ctrl.step(sensor_reading=sense,weights=weights_ctrl)
			print a
			if give_it_a_push % 500:
				a[:3] *=1.5
			give_it_a_push+=1
			# print a
			# env.performActionNew(a,reshape_func=re.reform)
			env.performAction(a)
			
			#world model weights delta rule with respect to ||xsi_t+1||^2
			sensor_pred = wrld.step(a,weights_wrld)
			
			sensor_real = re.reform(env.getSensorByName("JointSensor"))

			"""
			possible solution for the guessing of the x-t-1 values
			"""
			line1.set_ydata([ (np.sum(n))/len(n) for n in weights_wrld[:,:-1] ] + weights_wrld[:,-1])

			ax.relim()
			ax.autoscale_view()

			line2.set_ydata([ (np.sum(n))/len(n) for n in weights_ctrl[:,:-1] ] + weights_ctrl[:,-1])
			#rescale
			bx.relim()
			bx.autoscale_view()
			fig.canvas.draw()

			#error minimization to world model weights
			xsi = sense - (weights_wrld[:,:-1] * ctrl.step(stateFull=False,old=2) + weights_wrld[:,-1].reshape(wrld.n,1))
			updt = ( xsi*(ctrl.step(stateFull=False,old=2).T) ) * wrld_lr
			weights_wrld[:,:-1] += updt
			sum_error = np.append(sum_error, xsi)
			sense_reverse, motor_reverse = ctrl.reverse_step(wrld.reverse_step(sensor_real)), wrld.reverse_step(sensor_real)
			#gradient descent to the controller weights
			tes = ctrl.learn(sense_reverse, motor_reverse, sensor_real, ctrl_lr)
			
			if np.isnan(np.min(tes)):
				print tes
				break
			if len(tes) != 1:
				weights_ctrl = tes
			
			inps = np.append(ctrl.sensor_buffer[(ctrl.t-1)%ctrl.buff_s],ctrl.sensor_buffer[(ctrl.t-2)%ctrl.buff_s])
			outs = np.append(sensor_real,motor_reverse)
			index, error = mlctrl.step(inps, outs)

		if not learning:
			#get the inputs for the net
			inps = np.append(ctrl.sensor_buffer[(ctrl.t-1)%ctrl.buff_s],ctrl.sensor_buffer[(ctrl.t-2)%ctrl.buff_s])
			#activate it
			print "\tactivating expert ",controling_expert_parameters[0]
			print "Active experts ",[x['id'] for x in controling_experts_on]
			output = np.zeros(len(sense)+env.indim)
			experts = 0
			for e in controling_experts_on:
				experts += float(e['id'])/len(controling_experts_on)
				output += np.asarray(mlctrl.nets[ e['id'] ].activate(inps))*(e['value']/10)
				print np.asarray(mlctrl.nets[ e['id'] ].activate(inps))*(e['value']/10)
				# print mlctrl.nets[ e['id'] ].activate(inps), inps, float(e['value'])/10, output
			# print output
			experts_sum.append(experts)
			sum_out = np.append(sum_out, output)
			sum_out = sum_out.reshape(len(sum_out)/len(output), len(output))

			#take outputs
			sens = output[:len(sense)]
			# print sens
			motor = output[len(sense):]
			# print motor
			#make the movement
			
			env.performActionNew(sens,reshape_func=re.reform)
			# env.performAction(motor)
			
			#store in buffers
			# sense = re.reform(env.getSensorByName("JointSensor"))
			ctrl.stepNoLearn(re.reform(env.getSensorByName('JointSensor')))



	'''
	TODO: rethink it
	#check movement from base, possible colision
	if chk!=None:
		print abs(chk-np.asarray(env.getSensorByName("DistToPointSensor")))
		if abs(chk-np.asarray(env.getSensorByName("DistToPointSensor"))) > 0.2 and wrld.t > reset_time+100 :
			reset_time = wrld.t
			env.reset() 
	chk = np.array(env.getSensorByName("DistToPointSensor"))




	for i in xrange(0,100):
		n = mlctrl.nets[1].activate(np.append(ctrl.sensor_buffer[(ctrl.t-1)% ctrl.buff_s], ctrl.sensor_buffer[(ctrl.t-2)% ctrl.buff_s]))
		env.performActionNew(n[15:],reshape_func=re.reform)
		er = re.reform(env.getSensorByName("JointSensor")) - n[:15]
	'''
		
	















#############################################################################
		# # compile the MSGD step into a theano function
		# updates = [(params, params - learning_rate * d_loss_wrt_params)]
		# MSGD = theano.function([batch], loss, updates=updates)

		
		# # here x_batch and y_batch are elements of train_batches and
		# # therefore numpy arrays; function MSGD also updates the params
		# print('Current loss is ', MSGD(weights_wrld))

		# def objective(x):
	#     A = np.array([
	#         [1, 0.8],
	#         [0.8, 1]])
	#     return np.sum(x * np.dot(x, A), axis=1)

	# x, y = np.meshgrid(np.linspace(-8, 8, 100), np.linspace(-8, 8, 100))
	# X = np.vstack([x.ravel(), y.ravel()]).T
	# z = objective(X)
	# plt.contour(x, y, z.reshape(x.shape));
	# plt.scatter(0,0,marker='x',linewidth=5,s=100,c='red');
	
	# x_start = np.random.uniform(-8, 8, size=(1,2))
	# x_path = optimize_with_autodiff(objective, x_start, T=50, eta=0.5)
	# plt.contour(x, y, z.reshape(x.shape));
	# plt.plot(x_path[:,0], x_path[:,1], marker='o');
	# plt.scatter(0,0,marker='x',linewidth=5,s=100,c='red');
	# plt.show()
