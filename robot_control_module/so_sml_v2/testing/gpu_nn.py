#!/usr/bin/env python

import theano
import theano.tensor as T
import numpy as np
import time

class NN:
	def __init__(self,):
		self.er = T.scalar("er")
		self.out = T.vector("out")
		self.w = theano.shared(np.asarray(np.random.random((4,10)),dtype=theano.config.floatX), name="w")
		self.inp = theano.shared(np.asarray(np.random.random(10),dtype=theano.config.floatX), name="inp")
		self.target = theano.shared(np.asarray([0.5,0.5,0.5,0.5],dtype=theano.config.floatX))
		self.params = []
		self.updates = []
	def step(self):
		self.params.append(self.w)
		self.inp = T.vector("inp")
		self.out = T.dot(self.w,self.inp)
		# for a in xrange(10):
		# 	T.set_subtensor(self.w[:a],self.w[:a]+0.01*(self.target-self.out)*self.inp)
		self.er = self.w/(self.target-self.out)
		# gparams = theano.gradient.grad(self.er, self.params)
		# updates=[]
		# for p,g in zip(self.params, gparams):
		# 	updates.append((p,p - g*0.01))

		f = theano.function(inputs=[self.inp], outputs=self.out, updates=([(self.w, self.w - self.er*0.01)]))
		return f 
	
if __name__ == "__main__":
	n = NN()
	
	#compiled step function
	net_step = n.step()
	t= time.time()
 	a = np.random.random(10)
	
	for _ in xrange(0,1000):
		
		inp = np.asarray(a,dtype=theano.config.floatX)
		#run compiled
		print net_step(inp)
	print time.time() - t

