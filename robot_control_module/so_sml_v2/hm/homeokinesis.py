#!/usr/bin/env python

import hm.world as world
import hm.controller as controller

#import numpy 
import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

#import our tools
from tools import sigmoid, dsigmoid

class Homeokinesis:
	def __init__(self, robot_sim, wlr, clr):
		
		#keep the robot
		self.robot_sim = robot_sim
		#get robots dimensions
		w_out_size = len(robot_sim.getSensorByName("JointSensor"))
		w_in_size = robot_sim.indim

		#make the DS for the world
		self.w = world.World(
			w_in_size, w_out_size, TT.nnet.sigmoid, dsigmoid, wlr
			)
		
		#make the DS for the controller
		self.c = controller.Controller(
			w_out_size, w_in_size, TT.nnet.sigmoid, dsigmoid, clr, bias=True
			)

		#array for the sensory readings t-1
		self.senses = []
		#array for controllers actions t-1
		self.actions = []
		#array for predicted sensors t
		self.pred_senses = []
		
		#compile theano functions
		self.controller_lr = clr
		self.controller_step = self.c.step()
		self.controller_learn = self.c.learn()

		self.world_lr = wlr
		self.world_step = self.w.step()
		self.world_learn = self.w.learn()

		print "homeokinesis:: inited"
		
		pass
	def step(self,):
		# import pdb; pdb.set_trace()
		#get readings
		self.senses.append(np.array(self.robot_sim.getSensorByName("JointSensor")))

		#make a step in the controller 
		self.actions.append(
			np.array(
				self.controller_step(self.senses[-1])
				)
			)

		#perform action
		self.robot_sim.performActionNew(
			(self.actions[-1])
			)#*2-1)*950)
		print " apply action, ",(self.actions[-1])

		#make a step in the world NN
		self.pred_senses.append(
			np.array(
				self.world_step(self.actions[-1])
				)
			)


		#get readings
		self.senses.append(
			np.array(
				self.robot_sim.getSensorByName("JointSensor")
				)
			)
		

		print "homeokinesis:: step sense:\n\t", self.senses[-2],\
			"\n\taction:\n\t",self.actions[-1]," \n\tsense:\n\t", self.senses[-1]


	def learn(self,):
		if len(self.senses) < 2 or len(self.actions) < 2:
			print "homeokinesis:: Not enough senses yet, length ", len(self.senses)
			return False

		
		#train world model on the difference between sens_real and predicted
		ksi_tp1 = LA.norm(self.senses[-1] - self.pred_senses[-1])**2
		self.world_learn(ksi_tp1, self.world_lr)

		#get the Jacobian of the world for training of the controller
		J = TT.gradient.jacobian(
			self.world_step(
				self.controller_step()
				),
			self.senses[-2]
			)
		L = LA.inv(J)
		u_t = LA.norm(L*ksi_tp1)**2
		self.controller_learn(u_t, self.controller_lr)
		
		return True
	def getJacobian(self, inp):
		'''
			calculate the error as the Jacobian of the outputs	
			and the inputs. The outputs of the world model
			and the inputs of the controller. This way we stabilize the
			whole network, and maintain a sqaure matrix for 
			the Jacobian
			J = partial Wold_out_j / partial in_i
		'''
		#normalize senses


		J = np.zeros((inp.shape[0],inp.shape[0]))
		for i in range(J.shape[0]):#going through the input size
			for j in range(J.shape[1]):#going through the output size
				the_sum = 0
				#all the weights from output i from world
				#self.w.weights[:,i]
				#all the weights form input j
				#self.c.weights[j,:]
				dv = []
				for ii in range(self.c.weights.shape[1]):
					d= self.c.df(np.sum(self.c.weights[:, ii] * inp))
					dv.append(d)
				for w_out, w_in,dvi in zip(self.w.weights[:,i],self.c.weights[j,:],dv):
					the_sum += w_out * w_in *dvi
				J[i,j] = the_sum

		return J



