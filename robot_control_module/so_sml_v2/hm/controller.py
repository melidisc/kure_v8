#!/usr/bin/env python

import theano
import theano.tensor as TT
import numpy as np
from numpy import linalg as LA


# from tools import logit

class Controller:
	def __init__(self, inp_s, out_s, f, df, lr, bias=True):
		
		self.f = f
		self.df = df
		self.inp_s = inp_s
		self.out_s = out_s
		self.bias = bias
		
		if self.bias:
			self.input_vec = theano.shared(
				np.ones(self.inp_s+1).astype(theano.config.floatX)
				)
			self.weights = theano.shared(
				np.random.uniform(0,1,(inp_s+1, out_s))
				)
		else:
			self.input_vec = theano.shared(
				np.ones(self.inp_s).astype(theano.config.floatX)
				)
			self.weights = theano.shared(
				np.random.uniform(0,1,(inp_s, out_s))
				)
		
		
		self.learning_rate = TT.scalar("controller_learning_rate")
		self.inp = TT.vector(name="input_controller")
		self.outp = TT.vector(name="output_controller")
		self.error_u_t = TT.scalar(name="error_u_t")
		self.target = TT.vector(name="target_controler_val")
		

		print "controller:: inited"

	def step(self,):
		
		self.input_vec = TT.set_subtensor(self.input_vec[:self.inp_s], self.inp)
		self.outp =  self.f(TT.dot(self.input_vec, self.weights))
		
		return theano.function(
			inputs=[self.inp],
			outputs=[self.outp])

	def learn(self,):
		world_step = TT.vector(name="worlds step")
		self.input_vec = TT.set_subtensor(self.input_vec[:self.inp_s],self.inp)
		self.outp =  self.f(TT.dot(self.input_vec, self.weights))

		J = theano.gradient.jacobian(world_step,self.target)
		L = LA.inv(J)
		u_t = LA.norm(L*ksi_tp1)**2

		#calculate gradient and apply
		gWeights = TT.grad(u_t, [self.weights])

		self.weights = self.weights - self.learning_rate*gWeights 
		
		return theano.function(
			inputs=[self.inp, self.target, world_step, self.learning_rate],
			outputs=[self.weights])


	def rev_step(self, inp, outp):
		return outp * np.linalg.inv(self.weights)
		
	def getJacobian(self, inp):
		#calculate the Jacobian and minimize the ||inverse||
		tmp = np.zeros((len(inp),len(self.weights)))
		for i,wi in enumerate(self.weights):
			#here we have weights per input
			for j,wj in enumerate(wi):
				tmp[i,j]= np.sum(self.df(wj*inp)*(1-self.df(wj*inp)))
		
		tmp_inv = np.linalg.inv(tmp)
		eigenval, eigenvect = np.linalg.eig(tmp)
		# print "controller:: eigenvals ", eigenval
		return tmp, tmp_inv,eigenval, eigenvect 

if __name__ == "__main__":
	def y(inp):
		return inp*2
	def yi(inp):
		return inp*2
	def sigmoid(inp):
		return 1/(1+np.exp(-inp))
	def dsigmoid(inp):
		return sigmoid(inp)*(1-sigmoid(inp))

	import matplotlib.pyplot as plt

	f = plt.figure()
	ax = f.add_subplot(411)
 	bx = f.add_subplot(412)
 	cx = f.add_subplot(413)
 	dx = f.add_subplot(414)
	s_inp = 4
	s_out = 4
	nn = Controller(s_inp, s_out,sigmoid,dsigmoid, 0.1)	
	l = np.linspace(0,150,num=150)
	dtst = [y(x) for x in l]
	# np.random.shuffle(dtst)
	ax.plot(dtst, "b-",label="Data")
	inps = []
	outps=[]
	errs=[]
	for _ in xrange(1):
		for ll, el in zip(l, dtst):
			inps.append(np.random.uniform(0.8,1,s_inp))
			#step
			outps.append(nn.step(inps[-1]))#np.tile(ll,4))
			#learn
			nn.learn(inps[-1], outps[-1], range(s_out))#np.tile(ll,4), np.tile(el,s_out)
			errs.append(nn.er)
	#plot the data
	
	bx.plot([x for x in inps],"g-",label="inputs")
	
	cx.plot([x for x in outps],"y-",label="outputs")
	
	dx.plot([x for x in errs],"b-",label="errors")
	
	# plt.legend()
	plt.show()

		
