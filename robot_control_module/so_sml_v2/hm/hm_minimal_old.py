import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

class hm_run():
		def __init__(self,c_inp_s, c_out_s, w_inp_s, w_out_s):
			#init variables
			self.c_inp_s = c_inp_s
			self.c_out_s = c_out_s
			self.w_inp_s = w_inp_s
			self.w_out_s = w_out_s
			#Controller variables
			self.controler_weights = theano.shared(
					np.random.uniform(-1,1,(c_inp_s+1, c_out_s))
					)
			self.controler_input_vec = theano.shared(
					np.ones(c_inp_s+1).astype(theano.config.floatX)
					)
			self.controler_lr = TT.scalar(name="controler lr")
			self.controler_out = TT.vector(name="controler output")
			#World variables
			self.world_weights = theano.shared(
					np.random.uniform(-1,1,(w_inp_s+1, w_out_s))
					)
			self.world_input_vec = theano.shared(
					np.ones(w_inp_s+1).astype(theano.config.floatX)
					)
			self.world_lr = TT.scalar(name="world lr")

			#Environment variables
			self.world_out = TT.vector(name="world output")
			self.sensors_t = TT.vector(name="sensors")
			self.motors_t = TT.vector(name="motors")
			self.sensors_tp1 = TT.vector(name="sensors tp1")
			self.real_sensors_tp1 = TT.vector(name="real sensors tp1")


		def controler_step(self,cin):
			self.controler_input_vec = TT.set_subtensor(
				self.controler_input_vec[:self.c_inp_s], cin
				)
			self.controler_out =  TT.tanh(
				TT.dot(self.controler_input_vec, self.controler_weights)
				)

			return self.controler_out
		def world_step(self,win):
			self.world_input_vec = TT.set_subtensor(
				self.world_input_vec[:self.w_inp_s], win
				)
			self.world_out =  TT.dot(self.world_input_vec, self.world_weights)

			return self.world_out
		
		def controler_step_f(self,):
			output = self.controler_step(self.sensors_t)
			return  theano.function(
				inputs=[self.sensors_t],
				outputs=[output])
		
		def world_step_f(self,):
			output = self.world_step(self.motors_t)

			return theano.function(
				inputs=[self.motors_t],
				outputs=[output])

		def run(self,):
			
			ksi_t = ((self.real_sensors_tp1 - self.world_out)**2).sum()

			###JACOBIAN###
			def the_loop(sens):
				self.controler_input_vec = TT.set_subtensor(
					self.controler_input_vec[:self.c_inp_s], sens
					)
				self.controler_out =  TT.tanh(
					TT.dot(self.controler_input_vec, self.controler_weights)
					)
				self.world_input_vec = TT.set_subtensor(
					self.world_input_vec[:self.w_inp_s], self.controler_out
					)
				wrld =  TT.dot(self.world_input_vec, self.world_weights)
				return wrld
			
			Jac = TT.jacobian(the_loop(self.sensors_t).flatten(), self.sensors_t)
			J = Jac
			###JACOBIAN###


			a =  TT.nlinalg.MatrixInverse()
			L = a(J)
			u_t = L*ksi_t
			u_t = (u_t**2).sum()

			gControlerWeights = TT.grad(u_t, [self.controler_weights])

			gWorldWeights = TT.grad(ksi_t, [self.world_weights])

			self.world_weights = self.world_weights - self.world_lr*gWorldWeights
			
			self.controler_weights = self.controler_weights - self.controler_lr*gControlerWeights

			runner = theano.function(
				inputs=[
					self.sensors_t,
					self.motors_t, 
					self.real_sensors_tp1, 
					self.controler_lr, 
					self.world_lr
				],
				outputs=[self.controler_weights, self.world_weights])
			
			return runner
