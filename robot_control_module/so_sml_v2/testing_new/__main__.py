#import homeokinesis modules
import homeokinesis_v2 as homeokinesis

#import robot instance modules
from instances import *

#import theano modules
import theano
import theano.tensor as T

import numpy as np

import matplotlib.pyplot as plt

from pybrain.rl.environments.ode.sensors import *

if __name__=="__main__":

    #load/init the robot
    print "here"
    robot_sim = instance_ball.BallEnv()#instance_ccrl_bare.CCRLEnv()#instance_acrobot.AcrobotEnv()#instance_arm.ArmEnv()#instance_octacrawl.OctacrawlEnv()#instance_octacrawl.OctacrawlEnv()# #instance_ccrl_bare.CCRLEnv()#
    robot_sim.addSensor(SpecificBodyPositionSensor(['body'], "bodyPos"))
    robot_sim.addSensor(SpecificBodyPositionSensor(['target'], "targetPos"))


    #world learning rate
    wlr = 0.25
    #controller learning rate
    clr = 1.

    h= homeokinesis.Homeokinesis(robot_sim, wlr, clr)

    plot_x_i = []
    plot_x_j = []
    plot_x = []
    plot_y = []

    cweights = []
    wweights = []
    for i in xrange(2000):
        
        x_tm1, x_t, x_tp1, m_t = h.step()

        if i == 100:
            target_pos = robot_sim.getSensorByName("targetPos")    

        print "Location ", robot_sim.getSensorByName("bodyPos")
        print "Target ", robot_sim.getSensorByName("targetPos"), 
        dst = np.sqrt(np.sum((np.array(robot_sim.getSensorByName("bodyPos")) \
            - np.array(robot_sim.getSensorByName("targetPos")))**2))
        print " Distance ", dst
        if i >100:
            print " REACHED ",any(abs(np.array(target_pos) - np.array(robot_sim.getSensorByName("targetPos")))>0.01) or \
                (dst<1.5)
        print i,
        if i>00:
            try:
                c,w,tle = h.learn()
            except np.linalg.LinAlgError as e:

                print "NOT ",x_tp1, e
                continue
            else:
                # print "FIRST OUT: ",c,"\nSECOND OUT:", w, " \nTLE ",tle
                cweights.append(c.flatten())
                wweights.append(w.flatten())

        plot_x_i.append(x_tm1)
        plot_x_j.append(x_t)
        plot_x.append(x_tp1)
        plot_y.append(m_t)

    f, axr = plt.subplots(4)
    axr[0].plot(plot_x_i)
    axr[0].set_ylabel("Sensor t-1")
    axr[1].plot(plot_x_j)
    axr[1].set_ylabel("Sensor t")
    axr[2].plot(plot_x)
    axr[2].set_ylabel("Sensor* t+1")
    axr[3].plot(plot_y)
    axr[3].set_ylabel("Motor* t")

    f, axr = plt.subplots(2)
    axr[0].plot(cweights)
    axr[0].set_ylabel("C_Weights")
    axr[1].plot(wweights)
    axr[1].set_ylabel("W_Weights")

    plt.show()
