# -*- coding: utf-8 -*-
"""
@author: Christs Melidis
"""
#Import Libraries:
import vrep                  #V-rep library
import sys
import time                #used to keep track of time
import numpy as np         #array library
import math
import matplotlib as mpl   #used for image plotting
from scipy import signal
import itertools

#Pre-Allocation

class VrepObj():
    def __init__(self,synchronous=False):

        vrep.simxFinish(-1) # just in case, close all opened connections
        self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
        self.sync = synchronous
        if self.sync:
            vrep.simxSynchronous(self.clientID, True)

        if self.clientID!=-1:  #check if client connection successful
            print 'Connected to remote API server'

        else:
            print 'Connection not successful'
            sys.exit('Could not connect')



        # get all joints and set them to zero
        errorCode, self.jointsarray = vrep.simxGetObjects(
            self.clientID,vrep.sim_object_joint_type,vrep.simx_opmode_blocking)
        if errorCode != 0:
            print " get all joints error", errorCode
            return False
        for j in self.jointsarray:
            errorCode = vrep.simxSetJointTargetPosition(
                self.clientID,j,0., vrep.simx_opmode_streaming)


        self.indim = len(self.jointsarray)
        self.actLen = self.indim

    def performActionNew(self,action,velocityControl=True):
        for j,a in zip(self.jointsarray,action):
                if velocityControl:
                    errorCode = vrep.simxSetJointTargetVelocity(
                        self.clientID,j,a, vrep.simx_opmode_streaming)
                else:
                    errorCode = vrep.simxSetJointTargetPosition(
                        self.clientID,j,a, vrep.simx_opmode_streaming)

        return True

    def getSensors(self,):
        returnCode,handles,_,floatData,_ = vrep.simxGetObjectGroupData(
            self.clientID,vrep.sim_object_joint_type,15,vrep.simx_opmode_streaming)


        if floatData == []:
            return np.zeros(self.indim)
        else:
            floatData = floatData[::2]
            return np.array(floatData)

    def getSensorByName(self,name):
        return self.getSensors()

    def step(self,):
        #Timing is kept through sleep

        #100Hz
        if self.sync:
            vrep.simxSynchronousTrigger(self.clientID)
        else:
            time.sleep(0.01)

        return True

    def close(self,):
        # Before closing the connection to V-REP,
        #make sure that the last command sent out had time to arrive.
        vrep.simxGetPingTime(self.clientID)

        # Now close the connection to V-REP:
        vrep.simxFinish(self.clientID)

    def reset(self,):
        return True

    def closeSocket(self,):
        return True
if __name__ == '__main__' :

    robot = VrepObj()

    t = np.linspace(0, 1, 200)
    sig = signal.sawtooth(2 * np.pi * 1 * t)
    sin_input = (s for s in itertools.cycle(sig))

    actions = []
    senses = []
    while True:
        try:
            act = []
            for j in xrange(robot.indim):
                act.append(sin_input.next())

            robot.performActionNew(act)
            actions.append(np.array(act))

            time.sleep(0.1)
            sensors = robot.getSensors()
            print "Sensor readings ", sensors
            senses.append(sensors)

        except KeyboardInterrupt:
            break

    robot.close()

    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(actions)
    plt.figure()
    plt.plot(senses)

    plt.show()
