__author__ = 'Christos Melidis, christos.melidis@plymouth.ac.uk'

from pybrain.rl.environments.ode import ODEEnvironment, sensors, actuators
import imp
from scipy import array,signal
import numpy as np
import math
import pdb, itertools

class CCRLEnv(ODEEnvironment):
    def __init__(self, renderer=True, realtime=True, ip="127.0.0.1", port="21590", buf='16384',stif=False):
        ODEEnvironment.__init__(self, renderer, realtime, ip, port, buf)
        # load model file
        # pdb.set_trace()
        if stif:
            self.loadXODE(imp.find_module('pybrain')[1] + "/rl/environments/ode/models/ccrl_bare_stif.xode")
        else:
            self.loadXODE(imp.find_module('pybrain')[1] + "/rl/environments/ode/models/ccrl_bare.xode")
        # standard sensors and actuators
        # pdb.set_trace()
        self.addSensor(sensors.JointSensor())
        self.addSensor(sensors.JointVelocitySensor())
        self.addActuator(actuators.JointActuator())
        # self.FricMu = 20.
        self.dt = 0.032
        #set act- and obsLength, the min/max angles and the relative max touques of the joints

        self.actLen = self.indim
        self.obsLen = len(self.getSensors())

        # self.tourqueList = array([  5., 8., 10., 8., 5.],)
        self.tourqueList = array([  1.]*self.indim,)*10.0
        # self.tourqueList = array([  .8, .0,.9, 1., .9, .8],)*10.0
        self.cHighList = array([ 1.0]*self.indim)*3.14
        self.cLowList = array([-1.0]*self.indim)*3.14

        self.sensor_limits = []
        # #Angle sensors
        # for i in range(self.actLen):
        #     self.sensor_limits.append((self.cLowList[i], self.cHighList[i]))
        # # Joint velocity sensors
        # for i in range(self.actLen):
        #     self.sensor_limits.append((-1, 1))
        # #Norm all actor dimensions to (-1, 1)
        # self.actor_limits = [(-10, 10)] * self.actLen

        self.stepsPerAction = 1
        self.I = .0
        self.D = 1.0
        self.I_max = 5.#self.tourqueList
        self.I_min = 5.#self.tourqueList
    def getObsLen(self):
        return self.obsLen
    def performActionNew(self,action, reshape_func=None):
        #Filtered mapping towards performAction of the underlying environment
        #The standard Johnnie task uses a PID controller to controll directly angles instead of forces
        #This makes most tasks much simpler to learn
        # action =  action - np.amin(action)
        # action = action / np.amax(action)

        action = np.array(action)
        isJoints=array(self.getSensorByName('JointSensor')) #The joint angles
        isSpeeds=array(self.getSensorByName('JointVelocitySensor')) #The joint angular velocitys

        act=(action+1.0)/2.0*(self.cHighList-self.cLowList)+self.cLowList #norm output to action intervall
        action=np.tanh((act-isJoints-isSpeeds)*16.0)*self.tourqueList #simple PID


        self.performAction(action)#*self.tourqueList)
        return action

if __name__ == '__main__' :
    w = CCRLEnv()
    # linspace = np.append(np.linspace(np.pi/6, 5*np.pi/6, 40),np.linspace(7*np.pi/6, 11*np.pi/6,40))
    # linspace_reverse = np.append(np.linspace(-np.pi/6, -5*np.pi/6, 40),np.linspace(-7*np.pi/6, -11*np.pi/6,40))
    # sin_input = ((np.sin(x)) for x,y in itertools.cycle(zip(linspace, linspace_reverse)))

    t = np.linspace(0, 1, 200)
    sig = signal.sawtooth(2 * np.pi * 1 * t)
    sin_input = (s for s in itertools.cycle(sig))
    actions = []
    senses = []
    print w.actLen
    while True:
        try:
            # if (w.stepCounter % 30) == 0:

            act = [sin_input.next()*np.random.random() for x in range(w.actLen)]
            # act += 0.2*np.random.random(2)-.1
            act = w.performActionNew(act)
            actions.append(act)
            print "Act", act

            sense = np.array(w.getSensorByName('JointSensor'))/3.14
            senses.append(sense)
            print "Sensors", sense

            w.step()
            # if w.stepCounter == 100000: w.reset()
        except KeyboardInterrupt:
            break

    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(actions)
    plt.figure()
    plt.plot(senses)

    plt.show()





# P = .1#.5*16.0
# self.I +=  error
# for i in self.I:
#     if i > self.I_max:
#         i = self.I_max
#     elif i < self.I_min:
#         i = self.I_min
# I_v = self.I * .05
# D_v = self.D * (error - self.D)
# action = P+I_v+D_v
# action=np.tanh((act-isJoints))*10.*self.tourqueList #simple PID
'''
isJoints=self.getSensorByName('JointSensor') #The joint angles
isSpeeds=self.getSensorByName('JointVelocitySensor') #The joint angular velocitys

dif = np.tanh(action-isJoints)*2.
for i in range(len(isSpeeds)):
    if np.absolute(isSpeeds[i]) < 0.5:
        dif[i] *= 40.

action = dif
'''
# self.performAction(act*self.tourqueList)
# print isSpeeds
# print F*self.tourqueList
# self.performAction(F*self.tourqueList)
# print "ACTION ", action
