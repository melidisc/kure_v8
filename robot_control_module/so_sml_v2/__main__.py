#!/usr/bin/env python

#import homeokinesis modules
import testing_new.homeokinesis_v2 as homeokinesis
# import hm.world as world
# import hm.controller as controller

#import robot instance modules
from instances import *

#import experts
from experts.expert import Experts

#import theano modules
import theano
import theano.tensor as T

import numpy as np

import matplotlib.pyplot as plt
if __name__=="__main__":

	#load/init the robot
	print "here"
	robot_sim =  instance_octacrawl.OctacrawlEnv()##instance_acrobot.AcrobotEnv()##instance_arm.ArmEnv()
	#world learning rate
	wlr = 0.01
	#controller learning rate
	clr = 0.1
	exp_num = 15
	ex_lr = 0.1

	h= homeokinesis.Homeokinesis(robot_sim, wlr, clr)
	exs = Experts(exp_num, 2*h.sensors, h.sensors+h.actuators, h.sensors)


	for step in xrange(10**4):
		#make a step
		x_tm1, x_t, x_tp1, m_t = h.step()

		inp = np.append(x_t, x_tm1)
		outp = np.append(x_tp1, m_t)
		#train the experts experts prediction
		exs.train(inp, outp, ex_lr)
		#learn from it
		h.learn()
	for e in exs.experts:
		plt.figure()
		e_out = []
		for el in e["dtset"]:
			inp,outp =  el
			# print e["step"](inp)
			e_out.append(e["step"](inp)[0])
		plt.plot(e_out)

	plt.show()
	exs.save("peos")
	exs.load("peos")
	for step in xrange(10):
		#make a step
		x_tm1, x_t, x_tp1, m_t = h.step()

		inp = np.append(x_t, x_tm1)
		outp = np.append(x_tp1, m_t)
		#train the experts experts prediction
		exs.train(inp, outp, ex_lr)
		#learn from it
		h.learn()
	# for step in xrange(10**5):
	# 	idd = np.random.randint(0,6)
	# 	if step == 0:
	# 		x_tm1, x_t, x_tp1, m_t = h.step()
	# 	else:
	# 		x_tm1, x_t, x_tp1, m_t = h.step(out[h.sensors:])

	# 	out = exs.step(inp, idd)
