import gsocketpool.pool
from mprpc import RPCServer,RPCPoolClient
import time,sys, gevent
from server import HubServerWrapper
from gevent.server import StreamServer
from gevent import monkey
monkey.patch_thread()
from threading import Thread

#

class HubPeer(RPCServer):#RPCServer
    # self.init(server_ip, server_port, me_who , ip, port)
    peers = []
    def init(self, H_ip, H_port, P_name, P_ip, P_port):
        self.me_who = P_name
        self.me_ip = P_ip
        self.me_port = P_port
        # self.listening = StreamServer((self.me_ip, self.me_port), HubPeerListener())
        # self.thread = Thread(target=self.listening.serve_forever)
        # self.thread.start()

        #now that i have set up my self, let the main know
        '''
        #How a subscriber should look like
        peers = {
            "who":None,
            "ip":None,
            "port":None,
            "contact":None}
        '''

    # def _close(self,):
    #     self.thread.join(2)

    def clean_who(self,who):
        who["who"] = str(who["who"])
        who["ip"] = str(who["ip"])
        who["port"] = int(who["port"])
        return who

    def peersUpdate(self, what, who):
        #clean who
        who = self.clean_who(who)
        #test the peer connect to him
        if what==1:#"new"
            print "Updating peer list.."
            print "New peer :",who
            peer_pool = gsocketpool.pool.Pool(
                RPCPoolClient, dict(host=who['ip'], port=who['port']))
            with peer_pool.connection() as client:
                print "Contacting ",who["who"]
                a = time.time()
                for i in range(9000):
                    client.call("ack",who)
                print "Time ", time.time()-a
            who['contact'] = peer_pool
            self.peers.append(who)
            del who

        if what==0:#"delete"
            for i_p,p in enumerate(self.peers):
                if who['who'] == p['who']:
                    self.peers.remove(i_p)

    def ack(self, msg):
        print msg

    def close(self,):
        print "Server down!"
#
# class HubPeerListener(RPCServer,HubPeer):
#     print "Listening .."
#     def ack(self, num):
#         print num
#


class HubPeerWrapper(HubPeer):
    def __init__(self,server_ip='127.0.0.1',server_port=6000,
            me_who='peer',ip='127.0.0.1',port=6001):
        # peer = HubPeer()
        self.ip = ip
        self.port = port
        self.who = me_who
        self.listening = StreamServer((ip, me_port), HubPeer("test"))
        # init(server_ip, server_port, me_who , ip, port)
        self.thread = None
        # self.listening.serve_forever

        self.mainserv_pool = gsocketpool.pool.Pool(
            RPCPoolClient, dict(host=server_ip, port=server_port))


    def start(self,):
        #start my server
        self.thread = Thread(target=self.listening.serve_forever)
        self.thread.start()
        #tell the main
        with self.mainserv_pool.connection() as client:
            print "I am ", self.who, " at ", self.ip,":",self.port
            client.call("subscribe",
                self.who,
                self.ip,
                self.port
                )
        print "HubPeer started at ",self.ip , " ", self.port
        # self.listening.serve_forever()
    def work(self,):
        for p in self.peers:
            print p

    def stop(self,):
        print "Stop"
        # self.thread.exit()
        self.listening.close()

if __name__=="__main__":
    ip = "127.0.0.1"
    print type(ip)
    port = int(sys.argv[2])
    me_who = str(sys.argv[3])
    me_port = int(sys.argv[4])
    peer = HubPeerWrapper(ip, port, me_who, ip, me_port)
    peer.start()
    while True:
        try:
            peer.work()
            gevent.sleep(0.1)
        except Exception as e:
            print e
    # print "HubPeer started at ",ip , " ", me_port
    # peer.join()
