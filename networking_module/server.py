from gevent.server import StreamServer
import gsocketpool.pool
from gsocketpool import TcpConnection
from mprpc import RPCServer,RPCPoolClient

class HubServer(RPCServer):#RPCServer
    subscribers = []
    ip = '127.0.0.1'
    port = 6000
    # def __init__(self,):
    '''
    #How a subscriber should look like
    subscriber = {
        "who":None,
        "ip":None,
        "port":None}
    '''
    def subscribe(self, who, ip, port):

        subscriber = {
            "who":str(who),
            "ip":str(ip),
            "port":int(port)}
        if subscriber not in self.subscribers:
            #meet him
            self._subscriberUpdate(1,subscriber)
            #greed him
            self.subscribers.append(subscriber)
        print "_subscriberUpdate ", self.subscribers

    def unsubscribe(self, who, ip, port):
        subscriber = {
            "who":who,
            "ip":ip,
            "port":port}
        try:
            #break up
            self._subscriberClose(who,ip,port)
            # id = self.subscribers.index(subscriber)
            self.subscribers.remove(subscriber)
        except Exception as e:
            print "Subscriber ",subscriber["who"] ," not found ",e
        else:
            print "Subscriber ",subscriber["who"]," removed!"
        #let everyone in the hood know
        self._subscriberUpdate(0,subscriber)

    def _subscriberUpdate(self,what, who):
            #init this Subscriber
            client_pool = gsocketpool.pool.Pool(
                RPCPoolClient, dict(host=who['ip'], port=who['port']))
            with client_pool.connection() as client:
                print "Initing  ", who['who'], " at ", who['ip'],\
                    ":",who['port']
                client.call("init",self.ip, self.port,
                    who['who'],
                    who['ip'],
                    who['port']
                    )
            #tell the others he is here
            for s in self.subscribers:
                client_pool = gsocketpool.pool.Pool(
                    RPCPoolClient, dict(host=s['ip'], port=s['port']))
                with client_pool.connection() as client:
                    client.call("peersUpdate",what, who)

    def _subscriberClose(self,who,ip,port):

        client_pool = gsocketpool.pool.Pool(
            RPCPoolClient, dict(host=ip, port=port))
        with client_pool.connection() as client:
            client.call("close")

class HubServerWrapper(HubServer):
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.server = StreamServer((self.ip, self.port), HubServer())

    def start(self,):
        print "HubServer started at ",self.ip , " ", self.port
        self.server.serve_forever()

    def stop(self,):
        for s in self.subscribers:
            self.unsubscribe(s['who'], s['ip'], s['port'])

if __name__=="__main__":

    server = HubServerWrapper('127.0.0.1',6000)
    try:
        server.start()
    except KeyboardInterrupt as e:
        server.stop()
        print "Stopped?"
