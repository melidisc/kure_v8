import logging 

def logger(func):
	def printer(*args, **kwargs):
		# FORMAT = "%(asctime)s %(name)-12s %(levelname)-8s %(message)s"
		# logging.basicConfig(format=FORMAT)
		# logger = logging.getLogger()
		print "\t Logger %s %s " %(args, kwargs)
		# print logger.info("Arguments: %s %s", (args, kwargs))
		return func(*args, **kwargs)
	return printer